<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('clinic_id')->nullable();
            $table->uuid('user_id')->nullable();
            $table->uuid('address_id')->nullable();
            $table->string('type');
            $table->string('medical_record_number')->nullable();
            $table->string('picture')->nullable();
            $table->string('name');
            $table->string('nickname')->nullable();
            $table->string('cpf')->nullable();
            $table->string('rg')->nullable();
            $table->string('job')->nullable();
            $table->date('birthday')->nullable();
            $table->string('gender')->nullable();
            $table->string('state')->nullable();
            $table->string('nationality')->nullable();
            $table->string('email')->nullable();
            $table->string('telephone')->nullable();
            $table->string('cellphone')->nullable();
            $table->string('cellphone2')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clinic_id')->references('id')->on('clinics');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('address_id')->references('id')->on('addresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
