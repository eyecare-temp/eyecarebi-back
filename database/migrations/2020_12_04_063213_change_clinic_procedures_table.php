<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeClinicProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_procedures', function (Blueprint $table) {
            $table->dropConstrainedForeignId('clinic_health_plan_id');
            $table->dropConstrainedForeignId('clinic_plan_id');
            $table->dropConstrainedForeignId('tuss_id');

            $table->uuid('clinic_id')->after('id');
            $table->uuid('procedure_id')->nullable()->after('clinic_id');
            $table->uuid('plan_id')->nullable()->after('procedure_id');
            $table->string('type')->nullable()->after('plan_id');

            $table->foreign('clinic_id')->references('id')->on('clinics');
            $table->foreign('procedure_id')->references('id')->on('procedures');
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_procedures', function (Blueprint $table) {
            $table->dropConstrainedForeignId('plan_id');
            $table->dropConstrainedForeignId('procedure_id');

            $table->uuid('clinic_health_plan_id')->nullable()->after('id');
            $table->uuid('clinic_plan_id')->nullable()->after('clinic_health_plan_id');
            $table->uuid('tuss_id')->after('clinic_plan_id');

            $table->foreign('clinic_health_plan_id')->references('id')->on('clinic_health_plans');
            $table->foreign('clinic_plan_id')->references('id')->on('clinic_plans');
            $table->foreign('tuss_id')->references('id')->on('procedures');
        });
    }
}
