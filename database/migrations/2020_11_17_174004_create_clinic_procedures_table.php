<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_procedures', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('clinic_health_plan_id')->nullable();
            $table->uuid('clinic_plan_id')->nullable();
            $table->uuid('tuss_id');
            $table->double('value')->nullable();
            $table->boolean('enabled')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clinic_health_plan_id')->references('id')->on('clinic_health_plans');
            $table->foreign('clinic_plan_id')->references('id')->on('clinic_plans');
            $table->foreign('tuss_id')->references('id')->on('tuss');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_procedures');
    }
}
