<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_plans', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('clinic_health_plan_id');
            $table->uuid('plan_id');
            $table->integer('return');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clinic_health_plan_id')->references('id')->on('clinic_health_plans');
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_plans');
    }
}
