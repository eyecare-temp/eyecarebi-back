<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTussTableToProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('tuss', 'procedures');

        Schema::table('procedures', function (Blueprint $table) {
            $table->dropColumn('tuss_code');
            $table->dropColumn('sus_code');
            $table->string('source')->after('id');
            $table->string('code')->after('source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('procedures', 'tuss');

        Schema::table('procedures', function (Blueprint $table) {
            $table->dropColumn('source');
            $table->dropColumn('code');
            $table->string('tuss_code')->after('id');
            $table->string('sus_code')->after('tuss_code')->nullable();
        });
    }
}
