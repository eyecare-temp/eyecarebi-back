<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHealthPlanIdToClinicProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_procedures', function (Blueprint $table) {
            $table->uuid('health_plan_id')->nullable()->after('procedure_id');

            $table->foreign('health_plan_id')->references('id')->on('health_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_procedures', function (Blueprint $table) {
            $table->dropConstrainedForeignId('health_plan_id');
        });
    }
}
