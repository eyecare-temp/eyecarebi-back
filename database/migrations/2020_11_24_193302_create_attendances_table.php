<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('clinic_id');
            $table->uuid('appointment_id')->nullable();
            $table->uuid('professional_id');
            $table->uuid('patient_id');
            $table->dateTimeTz('start_datetime');
            $table->dateTimeTz('end_datetime')->nullable();
            $table->string('duration')->nullable();
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('clinic_id')->references('id')->on('clinics');
            $table->foreign('appointment_id')->references('id')->on('appointments');
            $table->foreign('professional_id')->references('id')->on('people');
            $table->foreign('patient_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
