<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReturnsToClinicProceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clinic_procedures', function (Blueprint $table) {
            $table->integer('return_health_plan')->nullable()->after('health_plan_id');
            $table->integer('return_plan')->nullable()->after('plan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clinic_procedures', function (Blueprint $table) {
            $table->dropColumn('return_health_plan');
            $table->dropColumn('return_plan');
        });
    }
}
