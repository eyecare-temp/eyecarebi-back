<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicines', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('cnpj')->nullable();
            $table->string('company_name');
            $table->string('anvisa_code')->nullable();
            $table->string('product_code')->nullable();
            $table->string('ean')->nullable();
            $table->string('product');
            $table->string('description');
            $table->string('cas')->nullable();
            $table->string('substance')->nullable();
            $table->string('type')->nullable();
            $table->boolean('restricted_to_hospitals')->default(0);
            $table->double('price_tax_free')->nullable();
            $table->double('price')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicines');
    }
}
