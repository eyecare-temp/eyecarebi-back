<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('search-crm', [\App\Http\Controllers\WebserviceCrmController::class, 'search']);

Route::post('register', \App\Http\Controllers\RegisterController::class);
Route::post('confirm', \App\Http\Controllers\ConfirmController::class);
Route::post('login', [\App\Http\Controllers\UserController::class, 'login']);

Route::post('forgot-password', [\App\Http\Controllers\ForgotPasswordController::class, 'request']);
Route::get('forgot-password', [\App\Http\Controllers\ForgotPasswordController::class, 'validateToken']);
Route::post('reset-password', [\App\Http\Controllers\ForgotPasswordController::class, 'reset']);

Route::get('check-invitation', [\App\Http\Controllers\InviteController::class, 'checkInvitation']);
Route::post('accept-invite', [\App\Http\Controllers\InviteController::class, 'acceptInvite']);

Route::post('contact', \App\Http\Controllers\ContactController::class);

Route::middleware('auth:api')->group(function() {
    Route::get('check-user', [\App\Http\Controllers\UserController::class, 'checkUser']);

    Route::get('my-data', [\App\Http\Controllers\PersonController::class, 'myData']);
    Route::post('my-data', [\App\Http\Controllers\PersonController::class, 'updateMyData']);

    Route::prefix('clinics/{clinic}')->group(function() {
        Route::apiResource('appointments', \App\Http\Controllers\AppointmentController::class)->except(['show']);

        Route::get('overview', \App\Http\Controllers\OverviewController::class);
        Route::get('load-form', \App\Http\Controllers\FormController::class);

        Route::apiResource('procedures', \App\Http\Controllers\ClinicProcedureController::class)->except(['show']);
        Route::apiResource('health-plans', \App\Http\Controllers\ClinicHealthPlanController::class);
        Route::apiResource('secretaries', \App\Http\Controllers\SecretaryController::class)->except(['show']);
        Route::apiResource('professionals', \App\Http\Controllers\ProfessionalController::class)->except(['show']);
        Route::apiResource('appointments', \App\Http\Controllers\AppointmentController::class)->except(['show']);

        Route::post('patients/quick-registration', [\App\Http\Controllers\PatientController::class, 'quickRegistration']);
        Route::get('patients/{patient}/attendance/{attendance}', [\App\Http\Controllers\AttendanceController::class, 'getAttendance']);
        Route::post('patients/{patient}/start-attendance', [\App\Http\Controllers\AttendanceController::class, 'startAttendance']);
        Route::put('patients/{patient}/attendance', [\App\Http\Controllers\AttendanceController::class, 'updateAttendance']);
        Route::get('patients/{patient}/dashboard', [\App\Http\Controllers\PatientController::class, 'dashboard']);
        Route::get('patients/{patient}/medical-history', [\App\Http\Controllers\PatientController::class, 'medicalHistory']);
        Route::apiResource('patients', \App\Http\Controllers\PatientController::class)->except(['destroy']);
    });

    Route::post('chats', [\App\Http\Controllers\ChatController::class, 'startChat']);
    Route::get('chats', [\App\Http\Controllers\ChatController::class, 'getChats']);
    Route::get('chats/{chat}', [\App\Http\Controllers\ChatController::class, 'getChat']);
    Route::post('chats/{chat}/upload-file', [\App\Http\Controllers\ChatController::class, 'uploadFile']);

    Route::apiResource('clinics', \App\Http\Controllers\ClinicController::class)->only(['show', 'update']);

    Route::get('procedures/search', \App\Http\Controllers\ProcedureController::class);
    Route::get('cid10', \App\Http\Controllers\Cid10Controller::class);
    Route::get('medicines', \App\Http\Controllers\MedicineController::class);
    Route::get('health-plans', \App\Http\Controllers\HealthPlanController::class);
    Route::get('health-plans/{healthPlan}/plans', \App\Http\Controllers\PlanController::class);
});

Route::get('chats/{chat}/get-file', [\App\Http\Controllers\ChatController::class, 'getFile']);

Route::prefix('glauco')->group(function() {
    Route::get('my-journey', \App\Http\Controllers\MyJourneyController::class);
});

Route::get('/test', function() {


    $file = \Illuminate\Support\Facades\Storage::disk('s3')->get('67563743_2626020914096697_145912253932109824_n.jpg');

});
