<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $person = \App\Models\Person::find('a98152c2-6b66-413f-a3d8-791d48bc8305');
//    $clinic = \App\Models\Clinic::find('3974e9c0-50ed-4ce9-ae2c-a1dcd9252348');

//    \Illuminate\Support\Facades\Mail::to('vitorm11@hotmail.com')->send(new \App\Mail\UserRegistered($clinic, $person));
//    return view('emails.user-registered');
});

Route::get('/tuss', function() {
    $rows = \Maatwebsite\Excel\Facades\Excel::toArray('', storage_path('app/public/tuss-oftalmo.xlsx'));
    foreach($rows[0] as $index => $row) {
        \App\Models\Procedure::create([
            'source' => 'TUSS',
            'code' => $row[0],
            'name' => $row[1]
        ]);
    }

   return response('Sucesso!');
});

Route::get('/mapeamento-tuss-sigtap', function() {
    $sigtap = 0;
    $rows = \Maatwebsite\Excel\Facades\Excel::toArray('', storage_path('app/public/mapeamento-tuss-sigtap.xlsx'));
    foreach($rows[0] as $index => $row) {
        $hasProcedure = \App\Models\Procedure::where([['source', 'TUSS'], ['code', $row[0]]])->count() > 0;
        if($hasProcedure && $row[2] != '' && $row[3] != '') {
            $codeIsNotAlreadyAdded = \App\Models\Procedure::where([['source', 'SIGTAP'], ['code', $row[2]]])->count() == 0;
            if($codeIsNotAlreadyAdded) {
                \App\Models\Procedure::create([
                    'source' => 'SIGTAP',
                    'code' => $row[2],
                    'name' => $row[3]
                ]);
                $sigtap++;
            }
        }
    }

    dd($sigtap);
});

Route::get('/operadoras', function() {
    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\HealthPlanImport, storage_path('app/public/operadoras.xlsx'));

    return response('Sucesso!');
});

Route::get('/planos', function() {
    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\PlanImport, storage_path('app/public/planos.xlsx'));

    return response('Sucesso!');
});

Route::get('/cid10-import', function() {
    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\Cid10Import, storage_path('app/public/cid10.xlsx'));

    return response('Sucesso!');
});

Route::get('/medicine-import', function() {
    \Maatwebsite\Excel\Facades\Excel::import(new \App\Imports\MedicineImport, storage_path('app/public/medicamentos.xlsx'));

    return response('Sucesso!');
});

Route::get('/medicine-brasindice-import', function() {
    $sheets = \Maatwebsite\Excel\Facades\Excel::toArray('', storage_path('app/public/medicamentos-oftalmo-brasindice.xlsx'));
    foreach($sheets[0] as $index => $row) {
        if($index > 0)
            \App\Models\Medicine::create([
                'company_name' => $row[0],
                'product' => $row[1],
                'description' => $row[2]
            ]);
    }

    return response('Sucesso!');
});
