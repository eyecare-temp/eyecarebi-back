<?php

namespace App\Mail;

use App\Models\Clinic;
use App\Models\Person;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $clinic;
    public $person;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Clinic $clinic, Person $person)
    {
        $this->clinic = $clinic;
        $this->person = $person;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user-registered');
    }
}
