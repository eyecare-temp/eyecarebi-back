<?php

namespace App\Mail;

use App\Models\Clinic;
use App\Models\Person;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class ProfessionalInvited extends Mailable
{
    use Queueable, SerializesModels;

    public $clinic;
    public $person;
    public $inviteId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Clinic $clinic, Person $person, $inviteId)
    {
        $this->clinic = $clinic;
        $this->person = $person;
        $this->inviteId = Crypt::encrypt($inviteId);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Você foi convidado(a) para acessar o EyecareBI da '.$this->clinic->name)->view('emails.professional-invited');
    }
}
