<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterPersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'name' => 'required',
            'cpf' => 'required|cpf',
            'crmState' => 'required',
            'crmNumber' => 'required',
            'cellphone' => 'required|celular_com_ddd',
            'zipCode' => 'required',
            'address' => 'required',
            'number' => 'required',
            'city' => 'required',
            'state' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'e-mail',
            'password' => 'senha',
            'name' => 'nome',
            'cpf' => 'CPF',
            'crmState' => 'estado do CRM',
            'crmNumber' => 'número do CRM',
            'cellphone' => 'celular',
            'zipCode' => 'CEP',
            'address' => 'endereço',
            'number' => 'número',
            'city' => 'cidade',
            'state' => 'estado',
        ];
    }
}
