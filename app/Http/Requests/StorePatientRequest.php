<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cpf' => 'required|cpf',
            'rg' => 'required',
            'job' => 'required',
            'birthday' => 'required|date_format:d/m/Y',
            'gender' => 'required|in:M,F',
            'marital_status' => 'required',
            'nationality' => 'required',
            'email' => 'required|email',
            'telephone' => 'required',
            'cellphone' => 'required|celular_com_ddd'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'cpf' => 'CPF',
            'rg' => 'RG',
            'job' => 'profissão',
            'birthday' => 'data de nascimento',
            'gender' => 'sexo',
            'marital_status' => 'estado civil',
            'nationality' => 'nacionalidade',
            'email' => 'e-mail',
            'telephone' => 'telefone',
            'cellphone' => 'celular'
        ];
    }
}
