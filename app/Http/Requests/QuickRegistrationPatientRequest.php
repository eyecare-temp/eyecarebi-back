<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuickRegistrationPatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cpf' => 'required|cpf',
            'birthday' => 'required|date_format:d/m/Y',
            'gender' => 'required|in:M,F',
            'email' => 'required|email',
            'cellphone' => 'required|celular_com_ddd'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'cpf' => 'CPF',
            'birthday' => 'data de nascimento',
            'gender' => 'sexo',
            'email' => 'e-mail',
            'cellphone' => 'celular'
        ];
    }
}
