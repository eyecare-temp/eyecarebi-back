<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreAppointmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patient.name' => 'required',
            'patient.cellphone' => 'required',
            'payment_method' => 'required|in:private,health_plan',
            'procedure' => 'required',
            'start_datetime' => 'required|date',
            'end_datetime' => 'required|date|after_or_equal:start_datetime',
            'health_plan' => 'required_if:payment_method,health_plan'
        ];
    }

    public function attributes()
    {
        return [
            'patient.name' => 'nome do paciente',
            'patient.cellphone' => 'celular do paciente',
            'payment_method' => 'método de pagamento',
            'procedure' => 'procedimento',
            'start_datetime' => 'data e horário de início',
            'end_datetime' => 'data e horário de término',
            'health_plan' => 'plano de saúde'
        ];
    }
}
