<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateClinicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'cnpj' => 'required|cnpj',
            'telephone' => 'required|celular_com_ddd',
            'email' => 'required|email',
            'address.zipCode' => 'required',
            'address.address' => 'required',
            'address.number' => 'required',
            'address.neighborhood' => 'required',
            'address.city' => 'required',
            'address.state' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nome',
            'cnpj' => 'CNPJ',
            'telephone' => 'telefone',
            'email' => 'e-mail',
            'address.zipCode' => 'CEP',
            'address.address' => 'endereço',
            'address.number' => 'número',
            'address.neighborhood' => 'bairro',
            'address.city' => 'cidade',
            'address.state' => 'estado'
        ];
    }
}
