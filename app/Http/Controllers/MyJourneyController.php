<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Attendance;
use App\Models\Crm;
use App\Models\Exam;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MyJourneyController extends Controller
{
    public function __invoke(Request $request) {
        $patient = Person::select('id')->where([['type', 'PATIENT'], ['cpf', $request->cpf]])->get();

        $nextAppointment = Appointment::whereIn('patient_id', $patient)->whereIn('status', ['SCHEDULED', 'CONFIRMED', 'DONE'])->where('start_datetime', '>', Carbon::now())->with(['clinic', 'professional'])->first();
        $attendances = Attendance::whereIn('patient_id', $patient)->whereIn('status', ['CLOSED', 'CLOSED_BY_SYSTEM'])->with(['appointment', 'professional', 'patient', 'professional.crms' => function($query) {
            return $query->where('main', true);
        }, 'medicalRecords' => function($query) {
            return $query->whereIn('type', ['queixa-principal', 'historia-da-doenca-atual', 'pressao-intraocular', 'solicitacao-de-exames', 'retorno', 'precricao-medicamentosa']);
        }])->orderBy('start_datetime', 'DESC')->get();
        $exams = Exam::whereIn('patient_id', $patient)->with(['files'])->get();

        $professionals = [];

        $eyecareHealthProfessional = Person::where('id', 'daaba427-8c2c-4a37-84eb-7f48e05aceb9')->first();
        if($eyecareHealthProfessional) {
            $professionals[$eyecareHealthProfessional->id] = $eyecareHealthProfessional;
        }

        $crm = Crm::where([['state', $request->crmUf], ['number', $request->crmNumber]])->first();
        if ($crm) {
            $professionals[$crm->person->id] = $crm->person;
        }

        return response()->json([
            'nextAppointment' => $nextAppointment,
            'attendances' => $attendances,
            'exams' => $exams,
            'professionals' => array_values($professionals),
        ]);
    }
}
