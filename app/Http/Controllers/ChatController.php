<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ChatController extends Controller
{
    protected function returnChatToken() {
        $personLogged = Auth::user()->person;

        $token = $this->cryptoJsAesEncrypt(env('GLAUCO_ENCRIPTION_KEY'), ['doctor_id' => $personLogged->id]);

        return base64_encode($token);
    }

    public function getChats() {
        $http = new Client();
        $response = $http->request('GET', env('GLAUCO_API_URL').'chats?token='.$this->returnChatToken());
        $responseBody = $response->getBody();

        $responseDecoded = json_decode($responseBody, true);

        return response()->json($responseDecoded);
    }

    public function getChat($chat) {
        $personLogged = Auth::user()->person;

        $http = new Client();
        $response = $http->request('GET', env('GLAUCO_API_URL').'chats/'.$chat.'?token='.$this->returnChatToken());
        $responseBody = $response->getBody();

        $responseDecoded = json_decode($responseBody, true);

        $clinic = $personLogged->clinics()->first();
        $userChat = $responseDecoded['user'];

        $patientOnEyecareBi = Person::where([['clinic_id', $clinic->id], ['type', 'PATIENT'], ['cpf', $userChat['cpf']], ['birthday', Carbon::parse($userChat['birthday'])->format('Y-m-d')]])->first();
        if ($patientOnEyecareBi) {
            $responseDecoded['user']['eyecarebi'] = $patientOnEyecareBi;
        }

        return response()->json($responseDecoded);
    }

    public function startChat(Request $request) {
        $personLogged = Auth::user()->person;

        $postData = [
            'patient_id' => $request->patient_id,
            'doctor_name' => $personLogged->name,
            'doctor_picture' => $personLogged->picture,
        ];

        $http = new Client();
        $response = $http->request('POST', env('GLAUCO_API_URL').'chats?token='.$this->returnChatToken(), [
            RequestOptions::FORM_PARAMS => $postData
        ]);
        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();
        $responseDecoded = json_decode($responseBody, true);

        return response()->json($responseDecoded, $responseStatus);
    }

    public function getFile(Request $request, $chat) {
        $fileNameExploded = explode('/', $request->fileName);

        if($fileNameExploded[0] !== $request->chat) {
            return response()->json([], 403);
        }

        $http = new Client();
        $response = $http->request('GET', env('GLAUCO_API_URL').'chats/'.$chat.'?boolean=1&token='.$request->chatToken);
        $responseBody = $response->getBody();

        $responseDecoded = json_decode($responseBody, true);

        if($responseDecoded['allowed']) {
            $file = Storage::disk('s3')->get($request->fileName);

            $headers = ['Content-Type' => $request->contentType];

            if($request->has('download') && $request->download === 1) {
                $headers = [
                    'Content-Description' => 'File Transfer',
                    'Content-Type' => 'application/octet-stream',
                    'Content-Disposition' => 'attachment; filename="'.$fileNameExploded[1].'"',
                    'Content-Transfer-Encoding' => 'binary',
                    'Expires' => '0',
                    'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0'
                ];
            }

            return response($file)->withHeaders($headers);
        }
    }

    public function uploadFile(Request $request, $chat) {
        $http = new Client();
        $response = $http->request('GET', env('GLAUCO_API_URL').'chats/'.$chat.'?boolean=1&token='.$this->returnChatToken());
        $responseBody = $response->getBody();

        $responseDecoded = json_decode($responseBody, true);

        if($responseDecoded['allowed']) {
            $file = $request->file('file');

            $fileUrl = Storage::disk('s3')->put($chat, $file);
            return response()->json([
                'contentType' => $file->getMimeType(),
                'fileUrl' => $fileUrl
            ]);
        }
    }

    /**
     * Decrypt data from a CryptoJS json encoding string
     *
     * @param mixed $passphrase
     * @param mixed $jsonString
     * @return mixed
     */
    protected function cryptoJsAesDecrypt($passphrase, $jsonString) {
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    /**
     * Encrypt value to a cryptojs compatiable json encoding string
     *
     * @param mixed $passphrase
     * @param mixed $value
     * @return string
     */
    protected function cryptoJsAesEncrypt($passphrase, $value) {
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx.$passphrase.$salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32,16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode($data);
    }
}
