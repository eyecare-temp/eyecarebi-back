<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexAppointmentsRequest;
use App\Http\Requests\StoreAppointmentsRequest;
use App\Http\Resources\AppointmentResource;
use App\Models\Appointment;
use App\Models\Clinic;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    public function index(Clinic $clinic, IndexAppointmentsRequest $request)
    {
        $personLogged = Auth::user()->person;
        $startDate = Carbon::parse($request->start)->startOfDay();
        $endDate = Carbon::parse($request->end)->endOfDay();
        $professional = null;

        if($personLogged->type === 'PROFESSIONAL') {
            $professional = $personLogged->id;
        } elseif($personLogged->type === 'SECRETARY') {
            $professional = $request->professional;
        }

        $appointments = Appointment::where([['clinic_id', $clinic->id], ['professional_id', $professional]]);

        if($startDate->diffInDays($endDate) > 0) {
            $appointments->where(function ($query) use ($startDate, $endDate) {
                $query->whereBetween('start_datetime', [$startDate, $endDate])->orWhere(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('end_datetime', [$startDate, $endDate])->orWhere(function ($query) use ($startDate, $endDate) {
                        $query->where([['start_datetime', '<=', $startDate], ['end_datetime', '>=', $endDate]]);
                    });
                });
            });
        } else {
            $appointments->whereDate('start_datetime', $startDate);
        }

        $appointments = $appointments->orderBy('start_datetime', 'ASC')->get();

        return response()->json(AppointmentResource::collection($appointments));
    }

    public function store(Clinic $clinic, StoreAppointmentsRequest $request)
    {
        $personLogged = Auth::user()->person;

        if($personLogged->type === 'PROFESSIONAL') {
            $professional = $personLogged->id;
        } elseif($personLogged->type === 'SECRETARY') {
            $professional = $request->professional;
        }

        $patient = Person::where([['clinic_id', $clinic->id], ['id', $request->patient['id']]])->first();

        $clinic->appointments()->create([
            'professional_id' => $professional,
            'patient_id' => $patient->id,
            'health_plan_id' => ($request->payment_method === 'health_plan')?$request->health_plan:null,
            'clinic_procedure_id' => $request->procedure,
            'payment_method' => $request->payment_method,
            'start_datetime' => $request->start_datetime,
            'end_datetime' => $request->end_datetime,
            'telemedicine_link'  => $request->telemedicine_link,
            'description' => $request->description,
            'status' => 'SCHEDULED'
        ]);

        return response()->json([], 201);
    }

    public function update(Request $request, Clinic $clinic, Appointment $appointment)
    {
        if($appointment->clinic_id === $clinic->id) {
            $appointment->health_plan_id = ($request->payment_method === 'health_plan')?$request->health_plan:null;
            $appointment->clinic_procedure_id = $request->procedure;
            $appointment->payment_method = $request->payment_method;
            $appointment->start_datetime = $request->start_datetime;
            $appointment->end_datetime = $request->end_datetime;
            $appointment->telemedicine_link  = $request->telemedicine_link;
            $appointment->description = $request->description;
            $appointment->save();

            return response()->json([]);
        }
    }

    public function destroy(Clinic $clinic, Appointment $appointment)
    {
        if($appointment->clinic_id === $clinic->id) {
            $appointment->delete();

            return response()->json([], 204);
        }
    }
}
