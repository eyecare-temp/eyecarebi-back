<?php

namespace App\Http\Controllers;

use App\Http\Resources\HealthPlanResource;
use App\Models\HealthPlan;
use Illuminate\Http\Request;

class HealthPlanController extends Controller
{
    public function __invoke(Request $request) {
        $healthPlans = HealthPlan::where('ans_code', 'like', '%'.$request->term.'%')->orWhere('company_name', 'like', '%'.$request->term.'%')->orWhere('fantasy_name', 'like', '%'.$request->term.'%')->limit(20)->get();

        return response()->json(HealthPlanResource::collection($healthPlans));
    }
}
