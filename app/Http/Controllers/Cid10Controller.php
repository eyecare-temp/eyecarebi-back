<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchCid10Request;
use App\Http\Resources\Cid10Resource;
use App\Models\Cid10;
use Illuminate\Http\Request;

class Cid10Controller extends Controller
{
    public function __invoke(SearchCid10Request $request)
    {
        $cid10 = Cid10::where('code', 'like', '%'.$request->term.'%')->orWhere('description', 'like', '%'.$request->term.'%')->limit(20)->orderBy('code')->get();

        return response()->json(Cid10Resource::collection($cid10));
    }
}
