<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfessionalResource;
use App\Mail\ProfessionalInvited;
use App\Models\Clinic;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ProfessionalController extends Controller
{
    public function index(Clinic $clinic) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $secretaries = $clinic->people()->where([['role', 'NOT LIKE', '%SECRETARY%'], ['role', '<>', 'MANAGER']])->get();

            return response()->json(ProfessionalResource::collection($secretaries));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function store(Clinic $clinic, Request $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $person = Person::create([
                'type' => $request->role,
                'name' => $request->name,
                'email' => $request->email,
            ]);
            $inviteId = Str::uuid();

            $clinic->people()->attach([$person->id => ['id' => $inviteId, 'role' => $request->role.'_INVITED']]);

            Mail::to($request->email)->send(new ProfessionalInvited($clinic, $person, $inviteId));

            return response()->json([], 201);
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function update(Clinic $clinic, Person $professional, Request $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $currentRole = $clinic->people()->where('person_id', $professional->id)->first()->pivot->role;
            if($request->enabled === 1) {
                $newRole = str_replace('INACTIVE', 'ACTIVE', $currentRole);
            } else {
                $newRole = str_replace('ACTIVE', 'INACTIVE', $currentRole);
            }

            $clinic->people()->updateExistingPivot($professional->id, ['role' => $newRole]);
            $professional->refresh();

            return response()->json(new ProfessionalResource($professional));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function destroy(Clinic $clinic, Person $professional) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $clinic->people()->detach($professional->id);

            return response()->json([], 204);
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }
}
