<?php

namespace App\Http\Controllers;

use App\Http\Resources\SecretaryResource;
use App\Mail\SecretaryInvited;
use App\Models\Clinic;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SecretaryController extends Controller
{
    public function index(Clinic $clinic) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $secretaries = $clinic->people()->where('role','like', '%SECRETARY%')->get();

            return response()->json(SecretaryResource::collection($secretaries));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function store(Clinic $clinic, Request $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $person = Person::create([
                'type' => 'SECRETARY',
                'name' => $request->name,
                'email' => $request->email,
            ]);
            $inviteId = Str::uuid();

            $clinic->people()->attach([$person->id => ['id' => $inviteId, 'role' => 'SECRETARY_INVITED']]);

            Mail::to($request->email)->send(new SecretaryInvited($clinic, $person, $inviteId));

            return response()->json([], 201);
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function update(Clinic $clinic, Person $secretary, Request $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            if($request->enabled === 1) {
                $status_text = 'SECRETARY_ACTIVE';
            } else {
                $status_text = 'SECRETARY_INACTIVE';
            }

            $clinic->people()->updateExistingPivot($secretary->id, ['role' => $status_text]);
            $secretary->refresh();

            return response()->json(new SecretaryResource($secretary));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function destroy(Clinic $clinic, Person $secretary) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $clinic->people()->detach($secretary->id);

            return response()->json([], 204);
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }
}
