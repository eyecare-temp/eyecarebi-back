<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\PersonResource;
use App\Models\Attendance;
use App\Models\Person;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Laravel\Passport\Client as OClient;

class UserController extends Controller
{
    public function login(LoginRequest $request) {
        $user = User::where('email', $request->email)->first();

        if($user && $user->email_verified_at != null) {
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $tokens = $this->getTokenAndRefreshToken($request->email, $request->password);
                $person = Person::where('user_id', $user->id)->first();

                $chatToken = $this->cryptoJsAesEncrypt(env('GLAUCO_ENCRIPTION_KEY'), [
                    'doctor_id' => $person->id,
                    'from' => [
                        'platform' => 'eyecarebi',
                        'id' => $person->id,
                        'name' => $person->name,
                        'picture' => $person->picture
                    ]
                ]);

                $responseCode = 200;
                $response = [
                    'accessToken' => $tokens['access_token'],
                    'refreshToken' => $tokens['refresh_token'],
                    'user' => new PersonResource($person),
                    'chatToken' => base64_encode($chatToken),
                ];
            } else {
                $responseCode = 400;
                $response = [
                    'error' => 'Verifique se seus dados de acesso foram digitados corretamente.'
                ];
            }
        } else {
            $responseCode = 400;
            $response = [
                'error' => 'Não reconhecemos este e-mail. Caso esteja correto, verifique se ele já foi confirmado.'
            ];
        }

        return response($response, $responseCode);
    }

    public function checkUser() {
        $personLogged = Auth::user()->person;
        $incompleteAttendance = null;

        $openedAttendances = $attendanceOpened = Attendance::where([['professional_id', $personLogged->id], ['status', 'OPENED']])->get();
        foreach($openedAttendances as $attendance) {
            $cid = $attendance->medicalRecords()->where([['type', 'cid']])->first();
            $retorno = $attendance->medicalRecords()->where([['type', 'retorno']])->first();

            if($cid && $retorno) {
                if($cid && count($cid->value) === 0) {
                    $incompleteAttendance = $attendance;
                }

                if($retorno && strlen($retorno->value['retorno']) === 0) {
                    $incompleteAttendance = $attendance;
                }
            } else {
                $incompleteAttendance = $attendance;
            }
        }

        $chatToken = $this->cryptoJsAesEncrypt(env('GLAUCO_ENCRIPTION_KEY'), [
            'doctor_id' => $personLogged->id,
            'from' => [
                'platform' => 'eyecarebi',
                'id' => $personLogged->id,
                'name' => $personLogged->name,
                'picture' => $personLogged->picture
            ]
        ]);

        return response()->json([
            'user' => new PersonResource($personLogged),
            'chatToken' => base64_encode($chatToken),
            'attendance' => ($incompleteAttendance !== null)?[
                'id' => $incompleteAttendance->id,
                'patient_id' => $incompleteAttendance->patient_id
            ]:null
        ]);
    }

    protected function getTokenAndRefreshToken($email, $password) {
        $oClient = OClient::where('password_client', 1)->first();
        $http = new Client();
        $response = $http->request('POST', url('/').'/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return $result;
    }

    /**
     * Decrypt data from a CryptoJS json encoding string
     *
     * @param mixed $passphrase
     * @param mixed $jsonString
     * @return mixed
     */
    function cryptoJsAesDecrypt($passphrase, $jsonString){
        $jsondata = json_decode($jsonString, true);
        $salt = hex2bin($jsondata["s"]);
        $ct = base64_decode($jsondata["ct"]);
        $iv  = hex2bin($jsondata["iv"]);
        $concatedPassphrase = $passphrase.$salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }

    /**
     * Encrypt value to a cryptojs compatiable json encoding string
     *
     * @param mixed $passphrase
     * @param mixed $value
     * @return string
     */
    function cryptoJsAesEncrypt($passphrase, $value){
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx.$passphrase.$salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv  = substr($salted, 32,16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return json_encode($data);
    }
}
