<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlanResource;
use App\Models\HealthPlan;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function __invoke(HealthPlan $healthPlan, Request $request) {
        $plans = $healthPlan->plans()->where(function($query) use ($request) {
            $query->where('ans_code', 'like', '%'.$request->term.'%')->orWhere('name', 'like', '%'.$request->term.'%');
        })->limit(20)->get();

        return response()->json(PlanResource::collection($plans));
    }
}
