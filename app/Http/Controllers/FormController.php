<?php

namespace App\Http\Controllers;

use App\Models\Clinic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    public function __invoke(Clinic $clinic) {
        $personLogged = Auth::user()->person;

        $forms = [];
        $form = $personLogged->forms()->where('clinic_id', $clinic->id)->orderBy('created_at', 'DESC')->first();
        if($form) {
            $forms = $form->forms;
        }

        return response()->json($forms);
    }
}
