<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmAccountRequest;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ConfirmController extends Controller
{
    public function __invoke(ConfirmAccountRequest $request) {
        if($request->code) {
            $personId = Crypt::decrypt($request->code);

            $person = Person::find($personId);
            if($person) {
                $personUser = $person->user;
                if($personUser) {
                    if($personUser->email_verified_at === null) {
                        $personUser->email_verified_at = Carbon::now();
                        $personUser->save();

                        return response()->json(['success' => true]);
                    } else {
                        return response()->json(['message' => 'Este usuário já foi verificado.', 'success' => false]);
                    }
                }
            } else {
                return response()->json(['message' => 'Usuário não encontrado!', 'success' => false]);
            }

//            $person = Person::find($request->code);
        }
    }
}
