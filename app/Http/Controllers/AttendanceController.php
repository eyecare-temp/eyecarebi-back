<?php

namespace App\Http\Controllers;

use App\Http\Resources\AttendanceResource;
use App\Models\Attendance;
use App\Models\Clinic;
use App\Models\Interval;
use App\Models\MedicalRecord;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{
    public function getAttendance(Clinic $clinic, Person $patient, Attendance $attendance, Request $request) {
        $personLogged = Auth::user()->person;

        if($personLogged->type === 'PROFESSIONAL' && $attendance->professional->id === $personLogged->id) {
            if($attendance->status == 'OPENED' && $request->has('start_interval')) {
                $canStartNewInterval = $attendance->intervals()->where('end_datetime', null)->count() === 0;

                if($canStartNewInterval) {
                    $attendance->intervals()->create([
                        'start_datetime' => Carbon::now()
                    ]);

                    $attendance->refresh();
                }
            }

            return response()->json(new AttendanceResource($attendance));
        }
    }

    public function startAttendance(Clinic $clinic, Person $patient, Request $request) {
        $personLogged = Auth::user()->person;

        if($personLogged->type === 'PROFESSIONAL') {
            $openedAttendance = Attendance::where([['clinic_id', $clinic->id], ['professional_id', $personLogged->id], ['patient_id', $patient->id], ['status', 'OPENED']])->first();

            if(!$openedAttendance) {
                $attendance = Attendance::create([
                    'clinic_id' => $clinic->id,
                    'appointment_id' => ($request->has('appointment'))?$request->appointment:null,
                    'professional_id' => $personLogged->id,
                    'patient_id' => $patient->id,
                    'start_datetime' => Carbon::now(),
                    'status' => 'OPENED'
                ]);

                $attendance->intervals()->create([
                    'start_datetime' => Carbon::now()
                ]);

                return response()->json(new AttendanceResource($attendance));
            } else {
                return response()->json(['id' => $openedAttendance->id, 'error' => 'Você não pode iniciar um novo atendimento para um paciente que já possui um atendimento em andamento.'], 422);
            }
        }
    }

    public function updateAttendance(Clinic $clinic, Person $patient, Request $request) {
        $personLogged = Auth::user()->person;

        if($personLogged->type === 'PROFESSIONAL') {
            $attendanceOpened = Attendance::where([['clinic_id', $clinic->id], ['professional_id', $personLogged->id], ['patient_id', $patient->id], ['status', 'OPENED']])->first();

            if($attendanceOpened) {
                if($request->action == 'start_interval') {
                    $canOpenNewInterval = Interval::where([['attendance_id', $attendanceOpened->id], ['end_datetime', null]])->count() === 0;

                    if($canOpenNewInterval) {
                        $attendanceOpened->intervals()->create([
                            'start_datetime' => Carbon::now()
                        ]);

                        return response()->json(['success' => true]);
                    } else {
                        return response()->json(['error' => 'Este atendimento já possui um intervalo em aberto.'], 422);
                    }
                } elseif($request->action == 'end_interval') {
                    $openedInterval = Interval::where([['attendance_id', $attendanceOpened->id], ['end_datetime', null]])->first();

                    if($openedInterval) {
                        $openedInterval->end_datetime = Carbon::now();
                        $openedInterval->save();

                        return response()->json(['success' => true]);
                    } else {
                        return response()->json(['error' => 'Este atendimento não possui nenhum intervalo aberto.'], 422);
                    }
                } elseif($request->action == 'end_attendance') {
                    $attendanceOpened->end_datetime = Carbon::now();
                    $attendanceOpened->status = $request->has('canceled')?'CANCELED':'CLOSED';
                    $attendanceOpened->save();

                    if($request->has('forms')) {
                        $personLogged->forms()->create([
                            'clinic_id' => $clinic->id,
                            'forms' => $request->forms
                        ]);
                    }

                    return response()->json(['success' => true]);
                } elseif($request->action == 'medical-record') {
                    $medicalRecord = MedicalRecord::where([['attendance_id', $attendanceOpened->id], ['type', $request->type]])->first();

                    if($medicalRecord) {
                        $medicalRecord->value = $request->value;
                        $medicalRecord->save();
                    } else {
                        $attendanceOpened->medicalRecords()->create([
                            'type' => $request->type,
                            'value' => $request->value
                        ]);
                    }

                    return response()->json(['success' => true]);
                } elseif($request->action == 'remove-medical-record') {
                    $medicalRecord = MedicalRecord::where([['attendance_id', $attendanceOpened->id], ['type', $request->type]])->first();

                    if($medicalRecord && strpos($medicalRecord->type, 'exame-personalizado-') !== false) {
                        $medicalRecord->delete();
                    }

                    return response()->json(['success' => true]);
                }
            } else {
                return response()->json(['error' => 'Você não possui um atendimento em andamento com este paciente.'], 422);
            }
        }
    }
}
