<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreClinicHealthPlanRequest;
use App\Http\Requests\UpdateClinicProcedureRequest;
use App\Http\Resources\ClinicHealthPlanResource;
use App\Http\Resources\HealthPlanResource;
use App\Http\Resources\PlanResource;
use App\Http\Resources\ProcedureResource;
use App\Models\Clinic;
use App\Models\ClinicHealthPlan;
use App\Models\ClinicProcedure;
use App\Models\Procedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClinicHealthPlanController extends Controller
{
    public function index(Clinic $clinic, Request $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $healthPlans = $clinic->healthPlans()->get();

            if($request->has('simple')) {
                $response = [];
                foreach ($healthPlans as $healthPlan) {
                    $response[] = new HealthPlanResource($healthPlan->healthPlan);
                }
            } else {
                $response = ClinicHealthPlanResource::collection($healthPlans);
            }

            return response()->json($response);
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function store(Clinic $clinic, StoreClinicHealthPlanRequest $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            foreach($request->plans as $plan) {
                if(count($plan['procedures']) > 0) {
                    foreach ($plan['procedures'] as $procedure) {
                        $clinic->procedures()->create([
                            'health_plan_id' => $request->health_plan,
                            'return_health_plan' => $request->return,
                            'plan_id' => $plan['plan'],
                            'return_plan' => $plan['return'],
                            'procedure_id' => $procedure['procedure'],
                            'value' => $procedure['value'],
                            'enabled' => $procedure['enabled']
                        ]);
                    }
                } else {
                    $clinic->procedures()->create([
                        'health_plan_id' => $request->health_plan,
                        'return_health_plan' => $request->return,
                        'plan_id' => $plan['plan'],
                        'return_plan' => $plan['return']
                    ]);
                }
            }

            foreach($request->procedures as $procedure) {
                $clinic->procedures()->create([
                    'health_plan_id' => $request->health_plan,
                    'return_health_plan' => $request->return,
                    'procedure_id' => $procedure['procedure'],
                    'value' => $procedure['value'],
                    'enabled' => $procedure['enabled']
                ]);
            }

            $healthPlanAlreadyAdded = $clinic->healthPlans()->where('health_plan_id', $request->health_plan)->count() > 0;
            if(!$healthPlanAlreadyAdded) {
                $clinic->healthPlans()->create([
                    'health_plan_id' => $request->health_plan,
                    'return' => $request->return
                ]);
            }

            return response()->json([], 201);
        } else {
            return response()->json(['error' => 'Você não tem permissão para inserir novo convênio'], 403);
        }
    }

    public function show(Clinic $clinic, $healthPlan) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $response = [
                'plans' => [],
                'procedures' => []
            ];

            $procedures = $clinic->procedures()->where('health_plan_id', $healthPlan)->get();

            foreach($procedures as $procedure) {
                if($procedure->plan && !array_key_exists($procedure->plan->id, $response['plans'])) {
                    $response['plans'][$procedure->plan->id]['plan'] = new PlanResource($procedure->plan);
                    $response['plans'][$procedure->plan->id]['return_plan'] = $procedure->return_plan;
                    $response['plans'][$procedure->plan->id]['procedures'] = [];
                }

                if($procedure->procedure) {
                    if($procedure->plan) {
                        $response['plans'][$procedure->plan->id]['procedures'][] = [
                            'id' => $procedure->id,
                            'procedure' => new ProcedureResource($procedure->procedure),
                            'value' => $procedure->value,
                            'enabled' => $procedure->enabled,
                        ];
                    } else {
                        $response['procedures'][] = [
                            'id' => $procedure->id,
                            'procedure' => new ProcedureResource($procedure->procedure),
                            'value' => $procedure->value,
                            'enabled' => $procedure->enabled,
                        ];
                    }
                }
            }

            $response['plans'] = array_values($response['plans']);

            return response()->json($response);
        } else {
            return response()->json(['error' => 'Você não tem permissão para alterar um convênio'], 403);
        }
    }

    public function update(Clinic $clinic, $healthPlan, Request $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $plans = [];
            $procedures = [];
            foreach($request->plans as $plan) {
                $plans[] = $plan['plan'];
                if(count($plan['procedures']) > 0) {
                    foreach ($plan['procedures'] as $procedure) {
                        if ($procedure['id'] !== null) {
                            $clinicProcedure = ClinicProcedure::find($procedure['id']);
                            if ($clinicProcedure) {
                                $clinicProcedure->return_health_plan = $request->return;
                                $clinicProcedure->return_plan = $plan['return'];
                                $clinicProcedure->value = $procedure['value'];
                                $clinicProcedure->enabled = $procedure['enabled'];
                                $clinicProcedure->save();
                            }
                        } else {
                            $clinic->procedures()->create([
                                'health_plan_id' => $request->health_plan,
                                'return_health_plan' => $request->return,
                                'plan_id' => $plan['plan'],
                                'return_plan' => $plan['return'],
                                'procedure_id' => $procedure['procedure'],
                                'value' => $procedure['value'],
                                'enabled' => $procedure['enabled']
                            ]);
                        }
                    }
                } else {
                    $planModel = $clinic->procedures()->where([['health_plan_id', $request->health_plan], ['plan_id', $plan['plan']]])->first();
                    if($planModel) {
                        $planModel->return_plan = $plan['return'];
                        $planModel->save();
                    } else {
                        $clinic->procedures()->create([
                            'health_plan_id' => $request->health_plan,
                            'return_health_plan' => $request->return,
                            'plan_id' => $plan['plan'],
                            'return_plan' => $plan['return']
                        ]);
                    }
                }
            }
            $clinic->procedures()->whereNotNull('health_plan_id')->whereNotIn('plan_id', $plans)->delete();

            foreach($request->procedures as $procedure) {
                if($procedure['id'] !== null) {
                    $clinicProcedure = ClinicProcedure::find($procedure['id']);
                    if($clinicProcedure) {
                        $clinicProcedure->return_health_plan = $request->return;
                        $clinicProcedure->value = $procedure['value'];
                        $clinicProcedure->enabled = $procedure['enabled'];
                        $clinicProcedure->save();
                    }
                } else {
                    $clinic->procedures()->create([
                        'health_plan_id' => $request->health_plan,
                        'return_health_plan' => $request->return,
                        'procedure_id' => $procedure['procedure'],
                        'value' => $procedure['value'],
                        'enabled' => $procedure['enabled']
                    ]);
                }
            }

            $clinicHealthPlan = $clinic->healthPlans()->where('health_plan_id', $request->health_plan)->first();
            if($clinicHealthPlan) {
                $clinicHealthPlan->return = $request->return;
                $clinicHealthPlan->save();
            }

            return response()->json([], 200);
        } else {
            return response()->json(['error' => 'Você não tem permissão para alterar um convênio'], 403);
        }
    }

    public function destroy(Clinic $clinic, $healthPlan) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $clinicHealthPlan = $clinic->healthPlans()->where('id', $healthPlan)->first();
            if($clinicHealthPlan) {
                $healthPlanId = $clinicHealthPlan->health_plan_id;

                $clinic->healthPlans()->where('health_plan_id', $healthPlanId)->delete();
                $clinic->procedures()->where('health_plan_id', $healthPlanId)->delete();

                return response()->json([], 204);

            }
        } else {
            return response()->json(['error' => 'Você não tem permissão para excluir um convênio'], 403);
        }
    }

//    public function index(Clinic $clinic) {
//        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();
//
//        if($userHasPermission) {
//            $healthPlans = $clinic->healthPlans;
//
//            return response()->json(ClinicHealthPlanResource::collection($healthPlans));
//        } else {
//            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
//        }
//    }
//
//    public function store(Clinic $clinic, Request $request) {
//        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();
//
//        if($userHasPermission) {
//            $clinicHealthPlan = $clinic->healthPlans()->create([
//                'health_plan_id' => $request->health_plan_id,
//                'return' => $request->return
//            ]);
//
//            foreach($request->plans as $plan) {
//                $clinicPlan = $clinicHealthPlan->plans()->create([
//                    'plan_id' => $plan['plan_id'],
//                    'return' => $plan['return']
//                ]);
//
//                foreach($plan['procedures'] as $procedure) {
//                    $clinicPlan->procedures()->create([
//                        'tuss_id' => $procedure['procedure_id'],
//                        'value' => $procedure['value'],
//                        'enabled' => $procedure['enabled']
//                    ]);
//                }
//            }
//
//            return response()->json([], 201);
//        } else {
//            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
//        }
//    }
//
//    public function update(Clinic $clinic, HealthPlan $healthPlan, Request $request) {
//        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();
//
//        if($userHasPermission) {
//
//        } else {
//            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
//        }
//    }
//
//    public function destroy(Clinic $clinic, HealthPlan $healthPlan) {
//        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();
//
//        if($userHasPermission) {
//            try {
//                $clinicHealthPlan = $clinic->healthPlans()->find($healthPlan->id);
//
//                foreach($clinicHealthPlan->plans as $plan) {
//                    foreach($plan->procedures as $procedure) {
//                        $procedure->delete();
//                    }
//                    $plan->delete();
//                }
//                $clinicHealthPlan->delete();
//
//                return response()->json([], 204);
//            } catch (\Exception $e) {
//                return response()->json($e);
//            }
//        } else {
//            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
//        }
//    }
}
