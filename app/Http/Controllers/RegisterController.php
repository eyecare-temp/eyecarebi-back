<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterPersonRequest;
use App\Models\Address;
use App\Models\Clinic;
use App\Models\Person;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function __invoke(RegisterPersonRequest $request) {
        try {
            DB::beginTransaction();

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            // Todos usuários validados
            $user->email_verified_at = Carbon::now();
            $user->save();

            $address = Address::create([
                'zipCode' => $request->zipCode,
                'address' => $request->address,
                'number' => $request->number,
                'complement' => $request->complement,
                'neighborhood' => $request->neighborhood,
                'city' => $request->city,
                'state' => $request->state,
                'country' => 'Brasil'
            ]);

            $person = Person::create([
                'user_id' => $user->id,
                'address_id' => $address->id,
                'type' => 'PROFESSIONAL',
                'name' => $request->name,
                'cpf' => $request->cpf,
                'email' => $request->email,
                'cellphone' => $request->cellphone,
            ]);

            $person->crms()->create([
                'state' => $request->crmState,
                'number' => $request->crmNumber,
                'main' => true
            ]);

            $clinic = Clinic::create([
                'name' => 'Clínica do(a) '.$request->name
            ]);

            $clinic->people()->sync([$person->id => ['id' => Str::uuid(), 'role' => 'MANAGER']]);

            DB::commit();

            return response()->json([], 201);
        } catch (\Exception $e) {
            DB::rollBack();

            dd($e);
        }
    }
}
