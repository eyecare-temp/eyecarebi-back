<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateClinicRequest;
use App\Http\Requests\UpdateClinicResource;
use App\Http\Resources\ClinicResource;
use App\Models\Clinic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ClinicController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function show(Clinic $clinic)
    {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            return response()->json(new ClinicResource($clinic));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Clinic  $clinic
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClinicRequest $request, Clinic $clinic)
    {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $clinic->fill($request->only(['name', 'cnpj', 'telephone', 'email']));

            if($request->hasFile('brand')) {
                $extension = $request->brand->extension();
                $upload = $request->brand->storeAs('public/images/clinics', $clinic->id.'.'.$extension);

                if($upload) {
                    $clinic->brand = $clinic->id.'.'.$extension;
                }
            }

            $clinic->save();

            if($clinic->address) {
                $address = $clinic->address;

                $address->zipCode = $request->address['zipCode'];
                $address->address = $request->address['address'];
                $address->number = $request->address['number'];
                $address->complement = $request->address['complement'];
                $address->neighborhood = $request->address['neighborhood'];
                $address->city = $request->address['city'];
                $address->state = $request->address['state'];
                $address->save();
            } else {
                $address = $clinic->address()->create([
                    'zipCode' => $request->address['zipCode'],
                    'address' => $request->address['address'],
                    'number' => $request->address['number'],
                    'complement' => $request->address['complement'],
                    'neighborhood' => $request->address['neighborhood'],
                    'city' => $request->address['city'],
                    'state' => $request->address['state'],
                    'country' => 'Brasil'
                ]);

                $clinic->address_id = $address->id;
                $clinic->save();
            }

            $clinic->refresh();

            return response()->json(new ClinicResource($clinic));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }
}
