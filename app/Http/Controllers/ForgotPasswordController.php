<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Mail\ForgotPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    public function request(Request $request) {
        $user = User::where('email', $request->email)->first();

        if($user) {
            $token = Str::uuid();

            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now()
            ]);

            Mail::to($request->email)->send(new ForgotPassword($token));
        }

        return response()->json();
    }

    public function validateToken(Request $request) {
        $token = Crypt::decrypt($request->token);

        $passwordResets = DB::table('password_resets')->where([['token', $token]])->first();
        if($passwordResets) {
            return response()->json([
                'email' => $passwordResets->email
            ]);
        }
    }

    public function reset(Request $request) {
        $token = Crypt::decrypt($request->token);

        $passwordResets = DB::table('password_resets')->where([['token', $token]])->first();
        if($passwordResets) {
            $user = User::where('email', $passwordResets->email)->first();
            if($user) {
                try {
                    DB::beginTransaction();

                    if($user->email_verified_at === null)
                        $user->email_verified_at = Carbon::now();

                    $user->password = Hash::make($request->password);
                    $user->save();

                    DB::table('password_resets')->where('token', $token)->delete();

                    foreach($user->tokens as $token) {
                        $token->revoke();
                    }

                    DB::commit();

                    $newRequest = new Request(['email' => $user->email, 'password' => $request->password]);
                    $response = app(UserController::class)->login(LoginRequest::createFromBase($newRequest));

                    return response()->json($response->original);
                } catch (\Exception $e) {
                    DB::rollBack();
                }
            }
        }
    }
}
