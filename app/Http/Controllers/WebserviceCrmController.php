<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchCrmRequest;
use Illuminate\Http\Request;

class WebserviceCrmController extends Controller
{
    public function search(SearchCrmRequest $request) {
        try {
            $soap = new \SoapClient('https://ws.cfm.org.br:8080/WebServiceConsultaMedicos/ServicoConsultaMedicos?wsdl');
            $result = $soap->__soapCall('Consultar', [
                'Consultar' => [
                    'uf' => $request->state,
                    'crm' => $request->number,
                    'chave' => 'X6TVXX38'
                ]
            ]);

            if((array)$result && $result->dadosMedico->situacao === 'A') {
                return response()->json([
                    'nome' => $result->dadosMedico->nome,
                ]);
            } else {
                return response()->json([
                   'error' => 'O CRM informado é inválido.'
                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'Não foi possível verificar o CRM.', 'm' => $e->getMessage(), 'r' => $result], 400);
        }
    }
}
