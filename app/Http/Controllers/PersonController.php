<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterPersonRequest;
use App\Http\Resources\PersonResource;
use App\Models\Address;
use App\Models\Clinic;
use App\Models\Person;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PersonController extends Controller
{
    public function myData() {
        $user = Auth::user();
        $person = $user->person;

        return response()->json(new PersonResource($person));
    }

    public function updateMyData(Request $request) {
        $user = Auth::user();
        $person = $user->person;

        if($request->type === 'personal') {
            $this->validate($request, [
                'gender' => 'required',
                'name' => 'required'
            ], [], [
                'gender' => 'titulo',
                'name' => 'nome'
            ]);

            $person->gender = $request->gender;
            $person->name = $request->name;
            $person->save();
        } elseif($request->type === 'password') {
            $this->validate($request, [
                'actual' => 'required',
                'new' => 'required|confirmed'
            ], [], [
                'actual' => 'senha atual',
                'new' => 'nova senha'
            ]);

            if(Hash::check($request->actual, Auth::user()->password)) {
                $user->password = bcrypt($request->new);
                $user->save();
            } else {
                return response()->json(['error' => 'A senha atual está incorreta.'], 422);
            }
        } elseif($request->type === 'crms') {
            $actualCrms = [];
            foreach ($request->crms as $crm) {
                if(!isset($crm['id']) || $crm['id'] == null) {
                    $crmSaved = $person->crms()->create($crm);
                } else {
                    $crmSaved = $person->crms()->where('id', $crm['id'])->first();
                    if($crmSaved) {
                        $crmSaved->fill([
                            'state' => $crm['state'],
                            'number' => $crm['number'],
                            'main' => $crm['main']
                        ]);
                        $crmSaved->save();
                    }
                }
                $actualCrms[] = $crmSaved->id;
            }
            $person->crms()->whereNotIn('id', $actualCrms)->delete();
        } elseif($request->type === 'cellphone') {
            $this->validate($request, [
                'cellphone' => 'required|celular_com_ddd',
            ], [], [
                'cellphone' => 'celular'
            ]);

            $person->cellphone = $request->cellphone;
            $person->save();
        } elseif($request->type === 'address') {
            $this->validate($request, [
                'zipCode' => 'required',
                'address' => 'required',
                'number' => 'required',
                'neighborhood' => 'required',
                'city' => 'required',
                'state' => 'required',
            ], [], [
                'zipCode' => 'CEP',
                'address' => 'endereço',
                'number' => 'número',
                'neighborhood' => 'bairro',
                'city' => 'cidade',
                'state' => 'estado',
            ]);

            if($person->address) {
                $address = $person->address;
                $address->zipCode = $request->zipCode;
                $address->address = $request->address;
                $address->number = $request->number;
                $address->complement = $request->complement;
                $address->neighborhood = $request->neighborhood;
                $address->city = $request->city;
                $address->state = $request->state;
                $address->save();
            } else {
                $address = $person->address()->create([
                    'zipCode' => $request->zipCode,
                    'address' => $request->address,
                    'number' => $request->number,
                    'complement' => $request->complement,
                    'neighborhood' => $request->neighborhood,
                    'city' => $request->city,
                    'state' => $request->state,
                    'country' => 'Brasil'
                ]);

                $person->address_id = $address->id;
                $person->save();
            }
        }
        $person = $person->refresh();

        return response()->json(new PersonResource($person));
    }
}
