<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuickRegistrationPatientRequest;
use App\Http\Requests\StorePatientRequest;
use App\Http\Requests\UpdatePatientRequest;
use App\Http\Resources\ExamResource;
use App\Http\Resources\PatientResource;
use App\Models\Address;
use App\Models\Clinic;
use App\Models\Person;
use App\Models\PersonPlan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    public function index(Clinic $clinic, Request $request) {
        $patients = Person::where([['clinic_id', $clinic->id], ['type', 'PATIENT']]);

        if ($request->type == 'search') {
            $patients = $patients->where(function($q) use ($request) {
                $q->where('name', 'like', '%'.$request->term.'%')->orWhereRaw(DB::raw('DATE_FORMAT(birthday, \'%d/%m/%Y\') like \'%'.$request->term.'%\''));
            });
        } else if ($request->type == 'letter') {
            $patients = $patients->where('name', 'like', $request->term.'%');
        }

        if($request->has('appointment')) {
            $patients = $patients->limit(20)->orderBy('name')->get();
            return response()->json(PatientResource::collection($patients));
        } else {
            $patients = $patients->orderBy('name')->paginate(50);
        }

        $totalOfPatients = Person::where([['clinic_id', $clinic->id], ['type', 'PATIENT']])->count();

        return response()->json([
            'total' => $totalOfPatients,
            'lastPage' => $patients->lastPage(),
            'patients' => PatientResource::collection($patients)
        ]);
    }

    public function store(Clinic $clinic, StorePatientRequest $request) {
        try {
            DB::beginTransaction();

            $lastMedicalRecordNumber = Person::where('clinic_id', $clinic->id)->max('medical_record_number');
            $nextMedicalRecordNumber = $lastMedicalRecordNumber + 1;

            $address = Address::create([
                'zipCode' => $request->address['zipCode'],
                'address' => $request->address['address'],
                'number' => $request->address['number'],
                'complement' => $request->address['complement'],
                'neighborhood' => $request->address['neighborhood'],
                'city' => $request->address['city'],
                'state' => $request->address['state'],
                'country' => 'Brasil'
            ]);

            $patient = Person::create([
                'clinic_id' => $clinic->id,
                'address_id' => $address->id,
                'type' => 'PATIENT',
                'medical_record_number' => $nextMedicalRecordNumber,
                'name' => $request->name,
                'nickname' => $request->nickname,
                'cpf' => $request->cpf,
                'rg' => $request->rg,
                'job' => $request->job,
                'birthday' => (!empty($request->birthday))?Carbon::createFromFormat('d/m/Y', $request->birthday):null,
                'gender' => $request->gender,
                'marital_status' => $request->marital_status,
                'nationality' => $request->nationality,
                'email' => $request->email,
                'telephone' => $request->telephone,
                'cellphone' => $request->cellphone,
                'cellphone2' => $request->cellphone2,
            ]);

            if($request->hasFile('picture')) {
                $extension = $request->picture->extension();
                $pictureFile = $patient->id.'.'.$extension;

                $upload = $request->picture->storeAs('public/images/people', $pictureFile);

                if($upload) {
                    $patient->picture = $pictureFile;
                    $patient->save();
                }
            }

            foreach($request->health_plans as $health_plan) {
                $patient->plans()->create([
                    'plan_id' => $health_plan['plan'],
                    'registration' => $health_plan['registration'],
                    'token' => $health_plan['token'],
                    'valid_until' => (!empty($health_plan['valid_until']))?Carbon::createFromFormat('d/m/Y', $health_plan['valid_until']):null,
                    'holder' => $health_plan['holder']
                ]);
            }

            DB::commit();

            return response()->json([], 201);
        } catch (\Exception $e) {
            DB::rollBack();

            dd($e);
        }
    }

    public function show(Clinic $clinic, Person $patient) {
        if($patient->clinic_id == $clinic->id) {
            return response()->json(new PatientResource($patient));
        }
    }

    public function update(Clinic $clinic, Person $patient, UpdatePatientRequest $request) {
        if($patient->clinic_id == $clinic->id) {
            $patient->name = $request->name;
            $patient->nickname = $request->nickname;
            $patient->cpf = $request->cpf;
            $patient->rg = $request->rg;
            $patient->job = $request->job;
            $patient->gender = $request->gender;
            $patient->marital_status = $request->marital_status;
            $patient->nationality = $request->nationality;
            $patient->email = $request->email;
            $patient->telephone = $request->telephone;
            $patient->cellphone = $request->cellphone;
            $patient->cellphone2 = $request->cellphone2;

            if((!empty($request->birthday)))
                $patient->birthday = Carbon::createFromFormat('d/m/Y', $request->birthday);

            if($request->hasFile('picture')) {
                $extension = $request->picture->extension();
                $pictureFile = $patient->id.'.'.$extension;

                $upload = $request->picture->storeAs('public/images/people', $pictureFile);

                if($upload) {
                    $patient->picture = $pictureFile;
                }
            }

            $address = $patient->address;
            if($address) {
                $address->zipCode = $request->address['zipCode'];
                $address->address = $request->address['address'];
                $address->number = $request->address['number'];
                $address->complement = $request->address['complement'];
                $address->neighborhood = $request->address['neighborhood'];
                $address->city = $request->address['city'];
                $address->state = $request->address['state'];
                $address->save();
            } else {
                $address = Address::create([
                    'zipCode' => $request->address['zipCode'],
                    'address' => $request->address['address'],
                    'number' => $request->address['number'],
                    'complement' => $request->address['complement'],
                    'neighborhood' => $request->address['neighborhood'],
                    'city' => $request->address['city'],
                    'state' => $request->address['state'],
                    'country' => 'Brasil'
                ]);

                $patient->address_id = $address->id;
            }

            $patient->save();

            $actualPlans = [];
            foreach($request->health_plans as $health_plan) {
                if(!isset($health_plan['id']) || $health_plan['id'] == null) {
                    $planSaved = $patient->plans()->create([
                        'plan_id' => $health_plan['plan'],
                        'registration' => $health_plan['registration'],
                        'token' => $health_plan['token'],
                        'valid_until' => (!empty($health_plan['valid_until']))?Carbon::createFromFormat('d/m/Y', $health_plan['valid_until']):null,
                        'holder' => $health_plan['holder']
                    ]);
                } else {
                    $planSaved = PersonPlan::find($health_plan['id']);
                    if($planSaved) {
                        $planSaved->fill([
                            'plan_id' => $health_plan['plan'],
                            'registration' => $health_plan['registration'],
                            'token' => $health_plan['token'],
                            'valid_until' => (!empty($health_plan['valid_until']))?Carbon::createFromFormat('d/m/Y', $health_plan['valid_until']):null,
                            'holder' => $health_plan['holder']
                        ]);
                        $planSaved->save();
                    }
                }
                $actualPlans[] = $planSaved->id;
            }
            $patient->plans()->whereNotIn('id', $actualPlans)->delete();
            $patient->refresh();

            return response()->json(new PatientResource($patient));
        }
    }

    public function dashboard(Clinic $clinic, Person $patient) {
        if($patient->clinic_id == $clinic->id) {
            $dashboardData = [
                'patient' => null,
                'doencasOftalmologicas' => [],
                'cirurgiasProcedimentosOftalmologicos' => [],
                'alergias' => [],
                'doencasPrevias' => [],
                'medicamentosEmUso' => [],
                'antecedentesFamiliaresOftalmologicos' => [],
                'acuidadeVisualERefracao' => [
                    'avcc' => null,
                    'avsc' => null,
                    'refracao' => null
                ],
                'prontuarios' => [],
                'historicoDeColirios' => [],
                'exames' => [],
                'pio' => [
                    'ultimos12meses' => [],
                    'ultimos36meses' => [],
                    'ultimos60meses' => [],
                ]
            ];

            $medicalRecords = $patient->medicalRecords;

            $dashboardData['patient'] = new PatientResource($patient);

            foreach ($medicalRecords as $medicalRecord) {
                if($medicalRecord->type === 'antecedentes-oftalmologicos') {
                    foreach($medicalRecord->value as $item) {
                        if(!in_array($item['antecedente'], $dashboardData['doencasOftalmologicas'])) {
                            $dashboardData['doencasOftalmologicas'][] = $item['antecedente'];
                        }
                    }
                } elseif($medicalRecord->type === 'cirurgias-oftalmologicas-anteriores') {
                    foreach($medicalRecord->value as $item) {
                        if(!in_array($item['cirurgia'], $dashboardData['cirurgiasProcedimentosOftalmologicos'])) {
                            $dashboardData['cirurgiasProcedimentosOftalmologicos'][] = $item['cirurgia'];
                        }
                    }
                } elseif($medicalRecord->type === 'alergias') {
                    foreach($medicalRecord->value as $item) {
                        if(!in_array($item['alergia'], $dashboardData['alergias'])) {
                            $dashboardData['alergias'][] = $item['alergia'];
                        }
                    }
                } elseif($medicalRecord->type === 'antecedentes-pessoais') {
                    foreach($medicalRecord->value as $item) {
                        if(!in_array($item['antecedente'], $dashboardData['doencasPrevias'])) {
                            $dashboardData['doencasPrevias'][] = $item['antecedente'];
                        }
                    }
                } elseif($medicalRecord->type === 'outros-medicamentos-em-uso') {
                    foreach($medicalRecord->value as $item) {
                        if($item['emUso'] && !in_array($item['medicamento'], $dashboardData['medicamentosEmUso'])) {
                            $dashboardData['medicamentosEmUso'][] = $item['medicamento'];
                        }
                    }
                } elseif($medicalRecord->type === 'antecedentes-familiares') {
                    foreach($medicalRecord->value as $item) {
                        if(!in_array($item['antecedente'], $dashboardData['antecedentesFamiliaresOftalmologicos'])) {
                            $dashboardData['antecedentesFamiliaresOftalmologicos'][] = $item['antecedente'];
                        }
                    }
                } elseif($medicalRecord->type === 'medicamentos-oftalmologicos-em-uso') {
                    foreach($medicalRecord->value as $item) {
                        if(!in_array($item['medicamento'], array_keys($dashboardData['historicoDeColirios']))) {
                            $dashboardData['historicoDeColirios'][$item['medicamento']] = $item;
                        }
                    }
                } elseif($medicalRecord->type === 'avsc-acuidade-visual-sem-correcao') {
                    if($dashboardData['acuidadeVisualERefracao']['avsc'] === null) {
                        $dashboardData['acuidadeVisualERefracao']['avsc'] = [
                            'data' => $medicalRecord->attendance->start_datetime->format('d/m/Y'),
                            'valor' => $medicalRecord->value
                        ];
                    }
                } elseif($medicalRecord->type === 'avcc-acuidade-visual-com-correcao') {
                    if($dashboardData['acuidadeVisualERefracao']['avcc'] === null) {
                        $dashboardData['acuidadeVisualERefracao']['avcc'] = [
                            'data' => $medicalRecord->attendance->start_datetime->format('d/m/Y'),
                            'valor' => $medicalRecord->value
                        ];
                    }
                } elseif($medicalRecord->type === 'refracao') {
                    if($dashboardData['acuidadeVisualERefracao']['refracao'] === null) {
                        $dashboardData['acuidadeVisualERefracao']['refracao'] = [
                            'data' => $medicalRecord->attendance->start_datetime->format('d/m/Y'),
                            'valor' => $medicalRecord->value
                        ];
                    }
                } elseif($medicalRecord->type === 'pressao-intraocular') {
                    $last12Months = Carbon::now()->subMonths(12)->startOfDay();
                    $last36Months = Carbon::now()->subMonths(36)->startOfDay();
                    $last60Months = Carbon::now()->subMonths(60)->startOfDay();
                    $attendanceStartDatetime = $medicalRecord->attendance->start_datetime;

                    if($attendanceStartDatetime > $last12Months) {
                        $dashboardData['pio']['ultimos12meses']['labels'][] = $attendanceStartDatetime->format('d/m/Y');
                        $dashboardData['pio']['ultimos12meses']['olhoEsquerdo'][] = floatval($medicalRecord->value['olhoEsquerdo']);
                        $dashboardData['pio']['ultimos12meses']['olhoDireito'][] = floatval($medicalRecord->value['olhoDireito']);

                        $dashboardData['pio']['ultimos36meses']['labels'][] = $attendanceStartDatetime->format('d/m/Y');
                        $dashboardData['pio']['ultimos36meses']['olhoEsquerdo'][] = floatval($medicalRecord->value['olhoEsquerdo']);
                        $dashboardData['pio']['ultimos36meses']['olhoDireito'][] = floatval($medicalRecord->value['olhoDireito']);

                        $dashboardData['pio']['ultimos60meses']['labels'][] = $attendanceStartDatetime->format('d/m/Y');
                        $dashboardData['pio']['ultimos60meses']['olhoEsquerdo'][] = floatval($medicalRecord->value['olhoEsquerdo']);
                        $dashboardData['pio']['ultimos60meses']['olhoDireito'][] = floatval($medicalRecord->value['olhoDireito']);
                    } elseif($attendanceStartDatetime > $last36Months && $attendanceStartDatetime < $last12Months) {
                        $dashboardData['pio']['ultimos36meses']['labels'][] = $attendanceStartDatetime->format('d/m/Y');
                        $dashboardData['pio']['ultimos36meses']['olhoEsquerdo'][] = floatval($medicalRecord->value['olhoEsquerdo']);
                        $dashboardData['pio']['ultimos36meses']['olhoDireito'][] = floatval($medicalRecord->value['olhoDireito']);

                        $dashboardData['pio']['ultimos60meses']['labels'][] = $attendanceStartDatetime->format('d/m/Y');
                        $dashboardData['pio']['ultimos60meses']['olhoEsquerdo'][] = floatval($medicalRecord->value['olhoEsquerdo']);
                        $dashboardData['pio']['ultimos60meses']['olhoDireito'][] = floatval($medicalRecord->value['olhoDireito']);
                    } elseif($attendanceStartDatetime > $last60Months && $attendanceStartDatetime < $last36Months) {
                        $dashboardData['pio']['ultimos60meses']['labels'][] = $attendanceStartDatetime->format('d/m/Y');
                        $dashboardData['pio']['ultimos60meses']['olhoEsquerdo'][] = floatval($medicalRecord->value['olhoEsquerdo']);
                        $dashboardData['pio']['ultimos60meses']['olhoDireito'][] = floatval($medicalRecord->value['olhoDireito']);
                    }
                }
            }

            if(isset($dashboardData['pio']['ultimos12meses']['labels']) && isset($dashboardData['pio']['ultimos12meses']['olhoEsquerdo']) && isset($dashboardData['pio']['ultimos12meses']['olho-direito'])) {
                $dashboardData['pio']['ultimos12meses']['labels'] = array_reverse($dashboardData['pio']['ultimos12meses']['labels']);
                $dashboardData['pio']['ultimos12meses']['olhoEsquerdo'] = array_reverse($dashboardData['pio']['ultimos12meses']['olhoEsquerdo']);
                $dashboardData['pio']['ultimos12meses']['olhoDireito'] = array_reverse($dashboardData['pio']['ultimos12meses']['olhoDireito']);
            }

            if(isset($dashboardData['pio']['ultimos36meses']['labels']) && isset($dashboardData['pio']['ultimos36meses']['olhoEsquerdo']) && isset($dashboardData['pio']['ultimos36meses']['olho-direito'])) {
                $dashboardData['pio']['ultimos36meses']['labels'] = array_reverse($dashboardData['pio']['ultimos36meses']['labels']);
                $dashboardData['pio']['ultimos36meses']['olhoEsquerdo'] = array_reverse($dashboardData['pio']['ultimos36meses']['olhoEsquerdo']);
                $dashboardData['pio']['ultimos36meses']['olhoDireito'] = array_reverse($dashboardData['pio']['ultimos36meses']['olhoDireito']);
            }

            if(isset($dashboardData['pio']['ultimos60meses']['labels']) && isset($dashboardData['pio']['ultimos60meses']['olhoEsquerdo']) && isset($dashboardData['pio']['ultimos60meses']['olho-direito'])) {
                $dashboardData['pio']['ultimos60meses']['labels'] = array_reverse($dashboardData['pio']['ultimos60meses']['labels']);
                $dashboardData['pio']['ultimos60meses']['olhoEsquerdo'] = array_reverse($dashboardData['pio']['ultimos60meses']['olhoEsquerdo']);
                $dashboardData['pio']['ultimos60meses']['olhoDireito'] = array_reverse($dashboardData['pio']['ultimos60meses']['olhoDireito']);
            }

            foreach($patient->exams as $exam) {
                $dashboardData['exames'][] = [
                    'name' => $exam->name,
                    'date' => $exam->date,
                ];
            }

            foreach($patient->attendances as $attendance) {
                $dashboardData['prontuarios'][] = [
                    'id' => $attendance->id,
                    'start_datetime' => $attendance->start_datetime,
                    'end_datetime' => ($attendance->end_datetime != null)?$attendance->end_datetime:null,
                    'status' => $attendance->status,
                    'cid' => $attendance->medicalRecords()->where('type', 'cid')->first()->value
                ];
            }

            return response()->json($dashboardData);
        }
    }

    public function medicalHistory(Clinic $clinic, Person $patient) {
        if($patient->clinic_id == $clinic->id) {
            $lastAttendance = $patient->attendances()->whereIn('status', ['CLOSED', 'CLOSED_AUTOMATIC'])->orderBy('start_datetime', 'DESC')->first();

            $medicalRecords = $lastAttendance->medicalRecords()->whereIn('type', ['antecedentes-oftalmologicos', 'antecedentes-pessoais', 'antecedentes-familiares', 'cirurgias-oftalmologicas-anteriores', 'habitos-de-vida', 'medicamentos-oftalmologicos-em-uso', 'outros-medicamentos-em-uso', 'alergias'])->get();

            $medicalHistory = [
                'medicalRecords' => $medicalRecords,
                'exams' => ExamResource::collection($patient->exams)
            ];

            return response()->json($medicalHistory);
        }
    }

    public function quickRegistration(Clinic $clinic, QuickRegistrationPatientRequest $request) {
        try {
            DB::beginTransaction();

            $lastMedicalRecordNumber = Person::where('clinic_id', $clinic->id)->max('medical_record_number');
            $nextMedicalRecordNumber = $lastMedicalRecordNumber + 1;

            $patient = Person::create([
                'clinic_id' => $clinic->id,
                'type' => 'PATIENT',
                'medical_record_number' => $nextMedicalRecordNumber,
                'name' => $request->name,
                'cpf' => $request->cpf,
                'birthday' => (!empty($request->birthday))?Carbon::createFromFormat('d/m/Y', $request->birthday):null,
                'gender' => $request->gender,
                'email' => $request->email,
                'cellphone' => $request->cellphone,
            ]);

            $patient->plans()->create([
                'plan_id' => $request->plan,
                'registration' => $request->registration
            ]);

            DB::commit();

            return response()->json(new PatientResource($patient), 201);
        } catch (\Exception $e) {
            DB::rollBack();

            dd($e);
        }
    }
}
