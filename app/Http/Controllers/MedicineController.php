<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchMedicineRequest;
use App\Http\Resources\MedicineResource;
use App\Models\Medicine;
use Illuminate\Http\Request;

class MedicineController extends Controller
{
    public function __invoke(SearchMedicineRequest $request) {
        $medicines = Medicine::where('product', 'like', '%'.$request->term.'%')->orWhere('description', 'like', '%'.$request->term.'%')->limit(20)->orderBy('product')->get();

        return response()->json(MedicineResource::collection($medicines));
    }
}
