<?php

namespace App\Http\Controllers;

use App\Http\Resources\AppointmentResource;
use App\Http\Resources\AttendanceResource;
use App\Models\Clinic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OverviewController extends Controller
{
    public function __invoke(Clinic $clinic) {
        $personLogged = Auth::user()->person;

        $overviewData = [];

        $todayAppointments = $clinic->appointments()->where([['professional_id', $personLogged->id]])->whereDate('start_datetime', '=', Carbon::today())->orderBy('start_datetime', 'ASC')->get();
        $overviewData['appointments'] = AppointmentResource::collection($todayAppointments->filter(function($appointment) {
            return $appointment->start_datetime >= Carbon::now();
        }));

        $overviewData['byType']['appointment'] = $todayAppointments->filter(function($appointment) {
            return $appointment->procedure->type === 'APPOINTMENT';
        })->count();

        $overviewData['byType']['telemedicine'] = $todayAppointments->filter(function($appointment) {
            return $appointment->procedure->type === 'TELEMEDICINE';
        })->count();

        $overviewData['byType']['return'] = $todayAppointments->filter(function($appointment) {
            return $appointment->procedure->type === 'RETURN';
        })->count();

        $overviewData['byType']['procedure'] = $todayAppointments->filter(function($appointment) {
            return $appointment->procedure->type === 'PROCEDURE';
        })->count();

        $last24Hours = Carbon::now()->subHours(24);
        $last24HoursAttendances = $personLogged->attendances()->whereDate('start_datetime', '>=', $last24Hours)->get();

        $overviewData['attendances'] = AttendanceResource::collection($last24HoursAttendances);

        return response()->json($overviewData);

        // Atendimentos das últimas 24h
    }
}
