<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Address;
use App\Models\Clinic;
use App\Models\Person;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class InviteController extends Controller
{
    public function checkInvitation(Request $request) {
        $inviteCode = Crypt::decrypt($request->code);

        $invite = DB::table('clinic_person')->where([['id', $inviteCode]])->whereIn('role', ['SECRETARY_INVITED', 'NURSE_INVITED', 'DOCTOR_INVITED', 'TECHNICIAN_INVITED', 'TECHNOLOGIST_INVITED'])->first();
        if($invite) {
            $clinic = Clinic::find($invite->clinic_id);
            $person = Person::find($invite->person_id);

            return response()->json([
                'clinic' => $clinic->name,
                'person' => [
                    'name' => $person->name,
                    'email' => $person->email
                ]
            ]);
        }
    }

    public function acceptInvite(Request $request) {
        $inviteCode = Crypt::decrypt($request->invitationCode);

        $invite = DB::table('clinic_person')->where([['id', $inviteCode]])->whereIn('role', ['SECRETARY_INVITED', 'NURSE_INVITED', 'DOCTOR_INVITED', 'TECHNICIAN_INVITED', 'TECHNOLOGIST_INVITED'])->first();
        if($invite) {
            $person = Person::find($invite->person_id);

            try {
                DB::beginTransaction();

                $user = User::create([
                    'name' => $request->name,
                    'email' => $person->email,
                    'password' => bcrypt($request->password)
                ]);

                $user->email_verified_at = Carbon::now();
                $user->save();

                $person->user_id = $user->id;
                $person->save();

                if($invite->role === 'DOCTOR_INVITED') {
                    $address = Address::create([
                        'zipCode' => $request->zipCode,
                        'address' => $request->address,
                        'number' => $request->number,
                        'complement' => $request->complement,
                        'neighborhood' => $request->neighborhood,
                        'city' => $request->city,
                        'state' => $request->state,
                        'country' => 'Brasil'
                    ]);

                    $person->type = 'PROFESSIONAL';
                    $person->address_id = $address->id;
                    $person->name = $request->name;
                    $person->cpf = $request->cpf;
                    $person->email = $request->email;
                    $person->cellphone = $request->cellphone;

                    $person->crms()->create([
                        'state' => $request->crmState,
                        'number' => $request->crmNumber,
                        'main' => true
                    ]);

                    $person->save();
                }

                DB::table('clinic_person')->where([['id', $inviteCode], ['role', $invite->role]])->update(['role' => str_replace('_INVITED', '', $invite->role)]);

                DB::commit();

                $request->replace(['email' => $person->email, 'password' => $request->password]);
                $response = app(UserController::class)->login(LoginRequest::createFromBase($request));

                return response()->json($response->original);
            } catch (\Exception $e) {
                DB::rollBack();

                dd($e);
            }

        }
        return response()->json($request->all());
    }
}
