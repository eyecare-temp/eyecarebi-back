<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchProcedureRequest;
use App\Http\Requests\StoreClinicProcedureRequest;
use App\Http\Requests\StoreProcedureRequest;
use App\Http\Requests\UpdateClinicProcedureRequest;
use App\Http\Resources\ClinicProcedureResource;
use App\Http\Resources\ProcedureResource;
use App\Models\Clinic;
use App\Models\ClinicProcedure;
use App\Models\Procedure;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClinicProcedureController extends Controller
{
    public function index(Clinic $clinic) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $procedures = $clinic->procedures()->whereNull('health_plan_id')->get();

            return response()->json(ClinicProcedureResource::collection($procedures));
        } else {
            return response()->json(['error' => 'Você não tem permissão para visualizar esta informação'], 403);
        }
    }

    public function store(Clinic $clinic, StoreClinicProcedureRequest $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $clinic->procedures()->create([
                'procedure_id' => $request->procedure,
                'type' => $request->type
            ]);

            return response()->json([], 201);
        } else {
            return response()->json(['error' => 'Você não tem permissão para inserir novo procedimento'], 403);
        }
    }

    public function update(Clinic $clinic, ClinicProcedure $procedure, UpdateClinicProcedureRequest $request) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $procedure->procedure_id = $request->procedure;
            $procedure->type = $request->type;
            $procedure->save();

            return response()->json([], 200);
        } else {
            return response()->json(['error' => 'Você não tem permissão para alterar um procedimento'], 403);
        }
    }

    public function destroy(Clinic $clinic, ClinicProcedure $procedure) {
        $userHasPermission = Auth::user()->person->clinics()->where([['clinic_id', $clinic->id]])->first();

        if($userHasPermission) {
            $procedure->delete();

            return response()->json([], 204);
        } else {
            return response()->json(['error' => 'Você não tem permissão para excluir um procedimento'], 403);
        }
    }
}
