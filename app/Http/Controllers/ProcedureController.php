<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchProcedureRequest;
use App\Http\Resources\ProcedureResource;
use App\Models\Procedure;

class ProcedureController extends Controller
{
    public function __invoke(SearchProcedureRequest $request) {
        $procedure = Procedure::where('source', $request->source);

        if($request->type === 'name') {
            $procedure = $procedure->where('name', 'like', '%'.$request->term.'%');
        } elseif($request->type === 'code') {
            $procedure = $procedure->where('code', 'like', '%'.$request->term.'%');
        }

        $procedure = $procedure->limit(20)->get();

        return response()->json(ProcedureResource::collection($procedure));
    }
}
