<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClinicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $clinic = [
            'id' => $this->id,
            'address' => ($this->address_id !== null)?[
                'zipCode' => $this->address->zipCode,
                'address' => $this->address->address,
                'number' => $this->address->number,
                'complement' => $this->address->complement,
                'neighborhood' => $this->address->neighborhood,
                'city' => $this->address->city,
                'state' => $this->address->state,
                'country' => $this->address->country
            ]:null,
            'brand' => $this->brand,
            'name' => $this->name,
            'cnpj' => $this->cnpj,
            'telephone' => $this->telephone,
            'email' => $this->email
        ];

        return $clinic;
    }
}
