<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClinicProcedureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $clinicProcedure = [
            'id' => $this->id,
            'clinic' => [
                'id' => $this->clinic->id,
                'name' => $this->clinic->name,
            ],
            'procedure' => new ProcedureResource($this->procedure),
        ];

        if($this->healthPlan && $this->plan) {
            $clinicProcedure['healthPlan'] = new HealthPlanResource($this->healthPlan);
            $clinicProcedure['plan'] = new PlanResource($this->plan);
            $clinicProcedure['value'] = $this->value;
            $clinicProcedure['enabled'] = $this->enabled;
        } else {
            $clinicProcedure['type'] = $this->type;
        }

        return $clinicProcedure;
    }
}
