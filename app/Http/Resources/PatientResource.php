<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $patient = [
            'id' => $this->id,
            'address' => ($this->address_id !== null)?[
                'zipCode' => $this->address->zipCode,
                'address' => $this->address->address,
                'number' => $this->address->number,
                'complement' => $this->address->complement,
                'neighborhood' => $this->address->neighborhood,
                'city' => $this->address->city,
                'state' => $this->address->state,
                'country' => $this->address->country
            ]:null,
            'medical_record_number' => $this->medical_record_number,
            'picture' => $this->picture,
            'name' => $this->name,
            'nickname' => $this->nickname,
            'cpf' => $this->cpf,
            'rg' => $this->rg,
            'job' => $this->job,
            'birthday' => ($this->birthday)?$this->birthday->format('d/m/Y'):null,
            'gender' => $this->gender,
            'marital_status' => $this->marital_status,
            'nationality' => $this->nationality,
            'email' => $this->email,
            'telephone' => $this->telephone,
            'cellphone' => $this->cellphone,
            'cellphone2' => $this->cellphone2,
            'health_plans' => []
        ];

        foreach($this->plans as $plan) {
            $patient['health_plans'][] = [
                'id' => $plan->id,
                'health_plan' => new HealthPlanResource($plan->plan->healthPlan),
                'plan' => new PlanResource($plan->plan),
                'registration' => $plan->registration,
                'token' => $plan->token,
                'valid_until' => ($plan->valid_until !== null)?$plan->valid_until->format('d/m/Y'):null,
                'holder' => $plan->holder
            ];
        }

        return $patient;
    }
}
