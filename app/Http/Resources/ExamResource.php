<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $files = [];
        foreach($this->files as $file) {
            if($file->type === 'pdf') {
//                $pdfFile = public_path('storage/exams/'.$file->file);
//                $pdf = chunk_split(base64_encode(file_get_contents($pdfFile)));
                $pdf = $file->file;
            }

            $files[] = [
              'id' => $file->id,
              'type' => $file->type,
              'file' => ($file->type === 'pdf')?$pdf:$file->file,
            ];
        }

        return [
            'id' => $this->id,
            'clinic' => [
                'id' => $this->clinic->id,
                'name' => $this->clinic->name
            ],
            'professional' => ($this->professional)?[
                'id' => $this->professional->id,
                'name' => $this->professional->name
            ]:null,
            'patient' => [
                'id' => $this->patient->id,
                'name' => $this->patient->name
            ],
            'name' => $this->name,
            'date' => $this->date,
            'files' => $files,
            'medical_report' => $this->medical_report
        ];
    }
}
