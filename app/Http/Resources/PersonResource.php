<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $person = [
            'id' => $this->id,
            'user' => ($this->user_id !== null)?[]:null,
            'address' => ($this->address_id !== null)?[
                'zipCode' => $this->address->zipCode,
                'address' => $this->address->address,
                'number' => $this->address->number,
                'complement' => $this->address->complement,
                'neighborhood' => $this->address->neighborhood,
                'city' => $this->address->city,
                'state' => $this->address->state,
                'country' => $this->address->country
            ]:null,
            'type' => $this->type,
            'picture' => $this->picture,
            'name' => $this->name,
            'nickname' => $this->nickname,
            'cpf' => '•••.•••.'.substr($this->cpf, -6),
            'rg' => $this->rg,
            'job' => $this->job,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'state' => $this->state,
            'nationality' => $this->nationality,
            'email' => $this->email,
            'telephone' => $this->telephone,
            'cellphone' => $this->cellphone,
            'cellphone2' => $this->cellphone2
        ];

        if($this->type === 'PROFESSIONAL') {
            foreach($this->crms as $crm) {
                $person['crms'][] = [
                    'id' => $crm->id,
                    'state' => $crm->state,
                    'number' => $crm->number,
                    'main' => $crm->main
                ];
            }
        }

        foreach($this->clinics as $clinic) {
            $person['clinics'][] = [
              'id' => $clinic->id,
              'name' => $clinic->name,
              'role' => $clinic->pivot->role
            ];
        }

        return $person;
    }
}
