<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HealthPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ans' => $this->ans_code,
            'cnpj' => $this->cnpj,
            'company_name' => $this->company_name,
            'fantasy_name' => $this->fantasy_name,
            'image' => $this->image
        ];
    }
}
