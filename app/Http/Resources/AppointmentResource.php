<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'clinic' => [
                'id' => $this->clinic->id,
                'name' => $this->clinic->name
            ],
            'professional' => [
                'id' => $this->professional->id,
                'name' => $this->professional->name
            ],
            'patient' => new PatientResource($this->patient),
            'health_plan' => ($this->health_plan_id !== null)?new HealthPlanResource($this->healthPlan):null,
            'procedure' => ($this->clinic_procedure_id !== null)?new ClinicProcedureResource($this->procedure):null,
            'payment_method' => $this->payment_method,
            'start_datetime' => $this->start_datetime->format('Y-m-d H:i:s'),
            'end_datetime' => $this->end_datetime->format('Y-m-d H:i:s'),
            'telemedicine' => $this->telemedicine,
            'telemedicine_link' => $this->telemedicine_link,
            'description' => $this->description,
            'status' => $this->status,
        ];
    }
}
