<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClinicHealthPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            'health_plan' => new HealthPlanResource($this->healthPlan),
            'return' => $this->return,
            'plans' => [],
            'procedures' => [],
        ];

        foreach ($this->plans as $plan) {
            $planToAdd = [
                'id' => $plan->id,
                'plan' => [
                    'id' => $plan->plan->id,
                    'ans' => $plan->plan->ans_code,
                    'name' => $plan->plan->name,
                ],
                'return' => $plan->return,
                'procedures' => []
            ];

            foreach($plan->procedures as $procedure) {
                $planToAdd['procedures'][] = [
                    'id' => $procedure->id,
                    'procedure' => [
                        'id' => $procedure->procedure->id,
                        'name' => $procedure->procedure->name
                    ],
                    'value' => $procedure->value,
                    'enabled' => $procedure->enabled,
                ];
            }

            $resource['plans'][] = $planToAdd;
        }

        return $resource;
    }
}
