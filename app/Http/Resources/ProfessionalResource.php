<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfessionalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'person' => [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email
            ],
            'status_text' => $this->whenPivotLoaded('clinic_person', function () {
                return $this->pivot->role;
            }),
            'status' => $this->whenPivotLoaded('clinic_person', function () {
                return (strpos($this->pivot->role, '_ACTIVE') !== false);
            })
        ];
    }
}
