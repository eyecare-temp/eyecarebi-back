<?php

namespace App\Http\Resources;

use App\Models\MedicalRecord;
use Illuminate\Http\Resources\Json\JsonResource;

class AttendanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attendance = [
            'id' => $this->id,
            'clinic' => new ClinicResource($this->clinic),
            'appointment' => ($this->appointment_id !== null)?'':null,
            'professional' => [
                'id' => $this->professional->id,
                'name' => $this->professional->name,
            ],
            'patient' => [
                'id' => $this->patient->id,
                'name' => $this->patient->name,
                'medical_record_number' => $this->patient->medical_record_number,
                'cpf' => $this->patient->cpf,
                'picture' => $this->patient->picture,
            ],
            'start_datetime' => $this->start_datetime,
            'end_datetime' => $this->end_datetime,
            'duration' => $this->duration,
            'status' => $this->status,
            'intervals' => IntervalResource::collection($this->intervals),
            'medicalRecords' => MedicalRecordResource::collection($this->medicalRecords)
        ];

        return $attendance;
    }
}
