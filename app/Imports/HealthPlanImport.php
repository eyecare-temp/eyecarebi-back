<?php

namespace App\Imports;

use App\Models\HealthPlan;

use Maatwebsite\Excel\Concerns\ToModel;

class HealthPlanImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function model(array $row)
    {
        return new HealthPlan([
            'ans_code' => $row[0],
            'cnpj' => $row[1],
            'company_name' => $row[2],
            'fantasy_name' => $row[3]
        ]);
    }

    public function rules()
    {
        return [
            '0' => function($attribute, $value, $onFailure) {
                if ($value === null || $value === '') {
                    $onFailure('Code is empty');
                }
            },
            '1' => function($attribute, $value, $onFailure) {
                if ($value === null || $value === '') {
                    $onFailure('CNPJ is empty');
                }
            },
            '2' => function($attribute, $value, $onFailure) {
                if ($value === null || $value === '') {
                    $onFailure('Company Name is empty');
                }
            },
            '3' => function($attribute, $value, $onFailure) {
                if ($value === null || $value === '') {
                    $onFailure('Fantasy Name is empty');
                }
            }
        ];
    }
}
