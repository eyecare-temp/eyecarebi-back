<?php

namespace App\Imports;

use App\Models\Cid10;
use App\Models\Medicine;
use Maatwebsite\Excel\Concerns\ToModel;

class MedicineImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        switch ($row[9]) {
            case 1:
                $type = 'Genérico';
                break;
            case 2:
                $type = 'Patente';
                break;
            case 3:
                $type = 'Referência';
                break;
            case 4:
                $type = 'Similar';
                break;
            case 5:
                $type = 'Novo';
                break;
            case 6:
                $type = 'Específico';
                break;
            case 7:
                $type = 'Biológicos';
                break;
            case 8:
                $type = 'Dinamizado';
                break;
            case 9:
                $type = 'Fitoterápico';
                break;
            case 10:
                $type = 'Radiofármaco';
                break;
            case 11:
                $type = 'Biológicos Novos';
                break;
            default:
                $type = $row[9];
        }

        return new Medicine([
            'cnpj' => $row[0],
            'company_name' => $row[1],
            'anvisa_code' => $row[2],
            'product_code' => $row[3],
            'ean' => $row[4],
            'product' => $row[5],
            'description' => $row[6],
            'cas' => $row[7],
            'substance' => $row[8],
            'type' => $type,
            'restricted_to_hospitals' => ($row[11] === 'S'),
            'price_tax_free' => floatval($row[14]),
            'price' => floatval($row[15])
        ]);
    }

    public function rules()
    {
        return [
            '0' => function($attribute, $value, $onFailure) {
                if ($value === '') {
                    $onFailure('Code is empty');
                }
            },
            '1' => function($attribute, $value, $onFailure) {
                if ($value === '') {
                    $onFailure('Description is empty');
                }
            }
        ];
    }
}
