<?php

namespace App\Imports;

use App\Models\HealthPlan;

use App\Models\Plan;
use Maatwebsite\Excel\Concerns\ToModel;

class PlanImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function model(array $row)
    {
        $healthPlan = HealthPlan::where('ans_code', $row[3])->first();

        if($healthPlan) {
            return new Plan([
                'health_plan_id' => $healthPlan->id,
                'ans_code' => $row[1],
                'name' => $row[2]
            ]);
        }
    }
}
