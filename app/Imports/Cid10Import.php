<?php

namespace App\Imports;

use App\Models\Cid10;
use Maatwebsite\Excel\Concerns\ToModel;

class Cid10Import implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Cid10([
            'code' => $row[0],
            'description' => $row[1]
        ]);
    }

    public function rules()
    {
        return [
            '0' => function($attribute, $value, $onFailure) {
                if ($value === '') {
                    $onFailure('Code is empty');
                }
            },
            '1' => function($attribute, $value, $onFailure) {
                if ($value === '') {
                    $onFailure('Description is empty');
                }
            }
        ];
    }
}
