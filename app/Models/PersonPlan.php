<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class PersonPlan extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['person_id', 'plan_id', 'registration', 'token', 'valid_until', 'holder'];
    protected $casts = ['valid_until' => 'date', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'deleted_at' => 'datetime'];

    public function person() {
        return $this->belongsTo(Person::class);
    }

    public function plan() {
        return $this->belongsTo(Plan::class);
    }

}
