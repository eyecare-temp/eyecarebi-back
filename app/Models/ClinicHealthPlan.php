<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class ClinicHealthPlan extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['clinic_id', 'health_plan_id', 'return'];

    public function clinic() {
        return $this->belongsTo(Clinic::class);
    }

    public function healthPlan() {
        return $this->belongsTo(HealthPlan::class);
    }

    public function plans() {
        return $this->hasMany(ClinicPlan::class);
    }

    public function procedures() {
        return $this->hasMany(ClinicProcedure::class);
    }
}
