<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class ClinicProcedure extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['health_plan_id', 'return_health_plan', 'plan_id', 'return_plan', 'procedure_id', 'type', 'value', 'enabled'];
    protected $casts = ['enabled' => 'boolean', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'deleted_at' => 'datetime'];

    public function clinic() {
        return $this->belongsTo(Clinic::class);
    }

    public function healthPlan() {
        return $this->belongsTo(HealthPlan::class);
    }

    public function plan() {
        return $this->belongsTo(Plan::class);
    }

    public function procedure() {
        return $this->belongsTo(Procedure::class);
    }
}
