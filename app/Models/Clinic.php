<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Clinic extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['address_id', 'brand', 'name', 'cnpj', 'telephone', 'email'];

    public function address() {
        return $this->belongsTo(Address::class);
    }

    public function people() {
        return $this->belongsToMany(Person::class)->withPivot(['id', 'role']);
    }

    public function procedures() {
        return $this->hasMany(ClinicProcedure::class);
    }

    public function healthPlans() {
        return $this->hasMany(ClinicHealthPlan::class);
    }

    public function appointments() {
        return $this->hasMany(Appointment::class);
    }
}
