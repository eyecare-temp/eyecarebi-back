<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class ClinicPlan extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['clinic_health_plan_id', 'plan_id', 'return'];

    public function clinicHealthPlan() {
        return $this->belongsTo(ClinicHealthPlan::class);
    }

    public function plan() {
        return $this->belongsTo(Plan::class);
    }

    public function procedures() {
        return $this->hasMany(ClinicProcedure::class);
    }
}
