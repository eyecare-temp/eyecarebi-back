<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Appointment extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['clinic_id', 'professional_id', 'patient_id', 'health_plan_id', 'clinic_procedure_id', 'payment_method', 'start_datetime', 'end_datetime', 'telemedicine', 'telemedicine_link', 'description', 'status'];
    protected $casts = ['start_datetime' => 'datetime', 'end_datetime' => 'datetime', 'telemedice' => 'boolean', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'deleted_at' => 'datetime'];

    public function clinic() {
        return $this->belongsTo(Clinic::class);
    }

    public function professional() {
        return $this->belongsTo(Person::class);
    }

    public function patient() {
        return $this->belongsTo(Person::class);
    }

    public function healthPlan() {
        return $this->belongsTo(HealthPlan::class);
    }

    public function procedure() {
        return $this->belongsTo(ClinicProcedure::class, 'clinic_procedure_id');
    }
}
