<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Attendance extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['clinic_id', 'appointment_id', 'professional_id', 'patient_id', 'start_datetime', 'end_datetime', 'duration', 'status'];
    protected $casts = ['start_datetime' => 'datetime', 'end_datetime' => 'datetime', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'deleted_at' => 'datetime'];

    public function clinic() {
        return $this->belongsTo(Clinic::class);
    }

    public function appointment() {
        return $this->belongsTo(Appointment::class);
    }

    public function professional() {
        return $this->belongsTo(Person::class, 'professional_id', 'id')->withTrashed();
    }

    public function patient() {
        return $this->belongsTo(Person::class, 'patient_id', 'id')->withTrashed();
    }

    public function intervals() {
        return $this->hasMany(Interval::class);
    }

    public function medicalRecords() {
        return $this->hasMany(MedicalRecord::class);
    }


}
