<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class Person extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['clinic_id', 'user_id', 'address_id', 'type', 'medical_record_number', 'picture', 'name', 'nickname', 'cpf', 'rg', 'job', 'birthday', 'gender', 'marital_status', 'nationality', 'email', 'telephone', 'cellphone', 'cellphone2'];
    protected $casts = ['birthday' => 'date', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'deleted_at' => 'datetime'];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function address() {
        return $this->belongsTo(Address::class);
    }

    public function crms() {
        return $this->hasMany(Crm::class);
    }

    public function clinics() {
        return $this->belongsToMany(Clinic::class)->withPivot(['role']);
    }

    public function plans() {
        return $this->hasMany(PersonPlan::class);
    }

    public function attendances() {
        $personLogged = Auth::user()->person;
        $foreignKey = ($this->type === 'PATIENT')?'patient_id':'professional_id';

        return $this->hasMany(Attendance::class, $foreignKey)->where([['professional_id', $personLogged->id], ['status', '<>', 'CANCELED']])->orderBy('start_datetime', 'DESC');
    }

    public function medicalRecords() {
        $personLogged = Auth::user()->person;
        $foreignKey = ($this->type === 'PATIENT')?'patient_id':'professional_id';

        return $this->hasManyThrough(MedicalRecord::class, Attendance::class, $foreignKey, 'attendance_id')->where([['attendances.professional_id', $personLogged->id], ['attendances.status', '<>', 'CANCELED']])->orderBy('attendances.start_datetime', 'DESC');
    }

    public function exams() {
        return $this->hasMany(Exam::class, 'patient_id')->orderBy('date', 'DESC');
    }

    public function forms() {
        return $this->hasMany(Form::class, 'professional_id');
    }

}
