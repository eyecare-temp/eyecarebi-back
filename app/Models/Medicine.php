<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Medicine extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['cnpj', 'company_name', 'anvisa_code', 'product_code', 'ean', 'product', 'description', 'cas', 'substance', 'type', 'restricted_to_hospitals', 'price_tax_free', 'price'];
}
