<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\Auditable;

class Exam extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = ['clinic_id', 'professional_id', 'patient_id', 'name', 'date', 'file', 'medical_report'];
    protected $casts = ['date' => 'date', 'created_at' => 'datetime', 'updated_at' => 'datetime', 'deleted_at' => 'datetime'];

    public function clinic() {
        return $this->belongsTo(Clinic::class);
    }

    public function professional() {
        return $this->belongsTo(Person::class, 'professional_id');
    }

    public function patient() {
        return $this->belongsTo(Person::class, 'patient_id');
    }

    public function files() {
        return $this->hasMany(File::class);
    }
}
