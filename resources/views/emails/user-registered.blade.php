<style type="text/css">
    @import url('https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@600;700&display=swap');
</style>

<div style="width: 100%; background-color: #F4F5FB; margin: 0; padding: 0; display: inline-block; font-family: 'Nunito Sans', sans-serif;">
    <div style="width: 580px; margin: 80px auto 30px auto; background-color: #FFFFFF;">
        <ul style="width: 580px; list-style-type: none; margin: 0; padding: 0;">
            <div style="width: 116px; height: 4px; background-color: #FF6B00; float: left; margin: 0; padding: 0;"></div>
            <div style="width: 116px; height: 4px; background-color: #FF9900; float: left; margin: 0; padding: 0;"></div>
            <div style="width: 116px; height: 4px; background-color: #FFCC18; float: left; margin: 0; padding: 0;"></div>
            <div style="width: 116px; height: 4px; background-color: #00C2FF; float: left; margin: 0; padding: 0;"></div>
            <div style="width: 116px; height: 4px; background-color: #305BF2; float: left; margin: 0; padding: 0;"></div>
        </ul>

        <div style="width: 100%; text-align: center; margin: 60px auto; display: inline-block">
            <img src="https://images.eyecarehealth.com.br/logo-colored.png" width="122">
        </div>

        <div style="padding: 0 32px 32px 32px; display: inline-block;">
            <h1 style="font-size: 24px; line-height: 31px; color: #0C1D59; margin: 0 0 24px 0; font-weight: 700;">Bem vindo, {{$person->name}}</h1>
            <p style="font-size: 16px; line-height: 24px; color: #525C7A; font-weight: 600;">
                Obrigado por se cadastrar no EyecareBI. Para confirmar o seu cadastro e começar a usar o sistema clique no botão abaixo:
            </p>

            <a href="https://sistema.eyecarehealth.com.br/confirmar-cadastro/{{$person->id}}" style="display: inline-block; margin: 40px 0; background-color: #305BF2; border-radius: 8px; padding: 14px; text-decoration: none; color: #FFFFFF;">Confirmar minha conta</a>

            <p style="font-size: 16px; line-height: 24px; color: #525C7A; font-weight: 600;">
                Qualquer dúvida, estamos à disposição.
            </p>

            <p style="font-size: 16px; line-height: 24px; color: #525C7A; font-weight: 600;">
                Equipe Eyecare Health
            </p>

            <p>
                <img src="https://images.eyecarehealth.com.br/logo-colored.png" width="122">
            </p>

            <p>
                <a href="mailto:contato@eyecarehealth.com.br" style="font-size: 16px; line-height: 24px; color: #305BF2; font-weight: 600; text-decoration: none;">
                    contato@eyecarehealth.com.br
                </a>

                <br/>

                <a href="https://www.eyecarehealth.com.br" target="_blank" style="font-size: 16px; line-height: 24px; color: #305BF2; font-weight: 600; text-decoration: none;">
                    www.eyecarehealth.com.br
                </a>
            </p>

            <div style="width: 100%; height: 1px; background-color: #D9DFF2; display: inline-block; margin: 24px 0;"></div>

            <p style="font-size: 14px; line-height: 21px; color: #8696AC;">
                Se você estiver com problema com o botão acima, copie e cole a URL abaixo no seu navegador<br/><br/>
                https://sistema.eyecarehealth.com.br/confirmar-cadastro/{{$person->id}}
            </p>
        </div>
    </div>

    <div style="width: 100%; text-align: center; display: inline-block; margin: 32px 0 80px 0; font-size: 14px; line-height: 21px; color: #8696AC;">
        <p style="margin: 0;">Eyecare Health. Todos os direitos reservados.</p>
    </div>
</div>
